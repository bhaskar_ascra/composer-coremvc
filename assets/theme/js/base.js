/* ----------- Horizontal Scroll on mouse wheel  ----------------*/
$(document).ready(function() {
  // ckeditor();
  // add_ckeditor_configuration();
  checkedShowsection();
  scrollbar();
  // selectpickerHelptext();
  // ckeditorHelptext();
  // helptexteditor();
  // toggleList();
  truncate();
  set_state_options_on_change_of_country_id();
  set_state_options();
  populate_state_options();
  set_city_options_on_change_of_state_id();
  set_city_options();
  populate_city_options();
  set_states();
  set_cities();
  // colorpicker();
  autocomplete_input(); 
  ajax_on_a_tag(); 
  ajax_post_on_tag();
  initialize_list_function();
  initialize_select_column_arrange_column();
  initialize_pagination();
  // imagesvideosFancybox();
  // default_tooltip();
  autocomplete_listing();
}).ajaxStop(function(){
  // ckeditor();
  // add_ckeditor_configuration();
  checkedShowsection();
  // selectpickerHelptext();
  // ckeditorHelptext();  
  // helptexteditor();
  selectpicker_refresh();
  autocomplete_listing();
  ajax_on_a_tag();
  // js_example();
  // colorpicker();
  // imagesvideosFancybox();
  // default_tooltip();
  // country_code_form();  
  //save_data_with_cropper_image();
  // save_helptext();
  // autocomplete_input();
  //modalCropper();
  // autofocus();
  initialize_select_column_arrange_column();
  initialize_pagination();
  setTimeout(function() {
     //initialize_list_function();
    //toggleList();
  }, 1000);
});




$(".modal").on('show.bs.modal', function(){
  dragAndDrop();
  // colorpicker();
  //save_data_with_cropper_image();
  setTimeout(function() {
    dragAndDrop();
    // autocomplete_input();
    // autofocus();
    // country_code_form();
    // formatted_phone_number();
  //save_data_with_cropper_image();
  }, 1000);
});
/* ----------- Horizontal Scroll on mouse wheel Close ----------------*/