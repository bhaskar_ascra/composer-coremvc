CREATE TABLE IF NOT EXISTS `states` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `country_id` int(11) NOT NULL DEFAULT '1',
  `created_at` datetime NULL,
  `updated_at` datetime NULL,
  `is_delete` tinyint(4)  NULL DEFAULT 0,
  `created_by` int(11) NULL,
  `updated_by` int(11) NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;