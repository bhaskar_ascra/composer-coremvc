CREATE TABLE IF NOT EXISTS `industries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NULL,
  `industry_type` varchar(255) NULL,
  `state` varchar(255) NULL,
  `city` varchar(255) NULL,
  `last_active_at` datetime NULL,
  `incorporated_at` datetime NULL,
  `total_area` float(10,4) NULL,
  `enabled` varchar(10) NULL,
  `status` varchar(50) NULL,
  `created_at` datetime NULL,
  `updated_at` datetime NULL,
  `is_delete` tinyint(4)  NULL DEFAULT 0,
  `created_by` varchar(255) NULL,
  `updated_by` varchar(255) NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;