CREATE TABLE IF NOT EXISTS `occupations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `occupation_name` varchar(255) NULL,
  `state` varchar(255) NULL,
  `city` varchar(255) NULL,
  `last_viewed_at` datetime NULL,
  `comments` text NULL,
  `enabled` varchar(10) NULL,
  `status` varchar(50) NULL,
  `created_at` datetime NULL,
  `updated_at` datetime NULL,
  `is_delete` tinyint(4)  NULL DEFAULT 0,
  `created_by` int(11) NULL,
  `updated_by` int(11) NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

ALTER TABLE `occupations` ADD `created_by` int(11) NULL; #Ignore if above queries are updated in database.
ALTER TABLE `occupations` ADD `updated_by` int(11) NULL; #Ignore if above queries are updated in database.