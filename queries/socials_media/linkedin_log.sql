CREATE TABLE IF NOT EXISTS `linkedin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(50) DEFAULT NULL,
  `is_share` varchar(10) DEFAULT NULL,
  `linkedin_title` varchar(255) DEFAULT NULL,
  `linkedin_link` text,
  `shared_by` varchar(50) DEFAULT NULL,
  `job_id` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `linkedin_response` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1039 ;