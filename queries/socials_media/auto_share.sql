CREATE TABLE IF NOT EXISTS `auto_share` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `linkedIn_share_id` varchar(255) NOT NULL,
  `email` varchar(200) DEFAULT NULL,
  `access_token` text,
  `status` varchar(100) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `error_msg` varchar(200) DEFAULT NULL,
  `error_at` datetime DEFAULT NULL,
  `error_date` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;