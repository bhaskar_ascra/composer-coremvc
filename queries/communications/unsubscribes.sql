CREATE TABLE IF NOT EXISTS `unsubscribes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(100) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_date` varchar(100) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `is_bounce` varchar(10) DEFAULT NULL,
  `is_disable` varchar(10) DEFAULT NULL,
  `sendgrid_status` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `email` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4593 ;