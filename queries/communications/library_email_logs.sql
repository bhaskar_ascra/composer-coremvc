CREATE TABLE IF NOT EXISTS `library_email_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hostname` varchar(255) NOT NULL,
  `toemail` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `fromemail` varchar(255) NOT NULL,
  `fromname` varchar(255) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `emailbody` longtext NOT NULL,
  `additiona_email_ids` text NOT NULL,
  `template_id` int(11) NOT NULL,
  `template_name` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `delivered_at` datetime DEFAULT NULL,
  `opened_at` datetime DEFAULT NULL,
  `clicked_at` datetime DEFAULT NULL,
  `emailhash` varchar(255) NOT NULL,
  `openemail` int(11) DEFAULT NULL,
  `openemailtime` datetime NOT NULL,
  `sendgrid_message_id` varchar(255) DEFAULT NULL,
  `sendgrid_status` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sendgrid_message_id` (`sendgrid_message_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=409064 ;

ALTER TABLE  `library_email_logs` ADD  `bounced_at` DATETIME NULL DEFAULT NULL AFTER  `clicked_at` ;
ALTER TABLE  `library_email_logs` ADD  `unsubscribe_at` DATETIME NULL DEFAULT NULL AFTER  `bounced_at` ;
ALTER TABLE  `library_email_logs` CHANGE  `emailto`  `toemail` VARCHAR( 255 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ;