sudo rm -r ~/projects/devops/application/core
sudo cp -r application/core ~/projects/devops/application/core

sudo rm ~/projects/devops/.htaccess
sudo cp .htaccess ~/projects/devops/.htaccess

sudo rm ~/projects/devops/.gitignore
sudo cp .gitignore ~/projects/devops/.gitignore

sudo rm -r ~/projects/devops/application/views/layouts/application
sudo cp -r application/views/layouts/application ~/projects/devops/application/views/layouts/application

sudo rm -r ~/projects/devops/application/views/layouts/login
sudo cp -r application/views/layouts/login ~/projects/devops/application/views/layouts/login

sudo rm -r ~/projects/devops/application/modules/sys
sudo cp -r application/modules/sys ~/projects/devops/application/modules/sys

sudo rm -r ~/projects/devops/application/modules/users
sudo cp -r application/modules/users ~/projects/devops/application/modules/users

sudo rm -r ~/projects/devops/application/modules/socials_media
sudo cp -r application/modules/socials_media ~/projects/devops/application/modules/socials_media

sudo rm -r ~/projects/devops/assets/core
sudo cp -r assets/core ~/projects/devops/assets/core

sudo rm -r ~/projects/devops/assets/login
sudo cp -r assets/login ~/projects/devops/assets/login
