<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

<script type="text/javascript" src="assets/lib/codemirror.js"></script>
<script src="assets/addon/edit/matchbrackets.js"></script>
<script src="assets/mode/htmlmixed/htmlmixed.js"></script>
<script src="assets/mode/xml/xml.js"></script>
<script src="assets/mode/css/css.js"></script>
<script src="assets/mode/clike/clike.js"></script>
<script src="assets/mode/php/php.js"></script>
<script src="assets/mode/javascript/javascript.js"></script>
<!-- 	<script src="keymap/sublime.js"></script> -->
<script>





// $("body").on('click', '.links a', function(e){
// 	// e.preventDefault();
// 	let filename = $(this).attr('href');
//   $("#page").load("tutorials/"+filename, function(responseTxt, statusTxt, xhr){
//     if(statusTxt == "success")
//       jseditor();
//     	navlinks();
//     if(statusTxt == "error")
//       alert("Error: " + xhr.status + ": " + xhr.statusText);
//   });
// });


	function jseditor(){
		var js_editor = document.getElementsByClassName("code");
		Array.prototype.forEach.call(js_editor, function(el) {
			// console.log(el.name)
		    var editor = CodeMirror.fromTextArea(el, {
		       lineNumbers: true,
			    // keyMap: "sublime",
			    // autoCloseBrackets: true,
			    mode: el.name,
			    matchBrackets: true,
			    showCursorWhenSelecting: true,
			    theme: "monokai",
			    tabSize: 2,
			    readOnly:true
		      });
		    // Update textarea
		    function updateTextArea() {
		        editor.save();
		    }
		    editor.on('change', updateTextArea);
		});	
	}

	$(document).ready(function(){
		 jseditor();
		 navlinks();
	});

	function navlinks(){
		let links = '<a href="index.php" class="homebtn">Home</a>';
		$('.editorbox').each(function(e){
			let thisid = $(this).attr('id');
			links += '<a href="#'+thisid+'">'+thisid+'</a>'
		});
		$('.sidebar').html(links).show();
		$('.headnav').show();

	}

$('body').on('click', '.sidebar a', function(){
	 $('.sidebar a').removeClass('active');
	$(this).addClass('active');
});
</script>

</body>
</html>