<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="assets/lib/codemirror.css">
	<!-- 	<link rel="stylesheet" href="addon/fold/foldgutter.css">
	<link rel="stylesheet" href="addon/dialog/dialog.css"> -->
	<link rel="stylesheet" href="assets/theme/monokai.css">
	<link rel="stylesheet" href="assets/style.css">
	<style>
		.CodeMirror {
	    border: 1px solid #eee;
	    height: auto;
	  }
  </style>
</head>
<body>