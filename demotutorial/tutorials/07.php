<h1>07 - Listing With Joins</h1>
<p>
  1. Join Fields in Listing<br \>
  2. Select Column<br \>
  3. Arrange Column<br \>
  4. Clear Filter<br \>
  5. Delete - http (SOFT)<br \>
</p>
<section id="Database" class="editorbox mt-5 mb-5">
  <h5>Migrations</h5>
  <p>Run these queries</p>
  <?php $file_path = 'application/modules/tutorials/migrations/20191025124853_create_table_employees.php'; include 'code.php'; ?>
  <?php $file_path = 'application/modules/tutorials/migrations/20191025161109_insert_into_employees.php'; include 'code.php'; ?>
</section>

<section id="Controller" class="editorbox mt-5 mb-5">
  <h5>Controller</h5>
  <?php $file_path = 'application/modules/tutorials/controllers/Employees.php'; include 'code.php'; ?>
</section>

<section id="Model" class="editorbox mt-5 mb-5">
  <h5>Model</h5>
  <?php $file_path = 'application/modules/tutorials/models/Employee_model.php'; include 'code.php'; ?>
</section>

<section id="Helper" class="editorbox mt-5 mb-5">
  <h5>Helper</h5>
  <?php $file_path = 'application/helpers/modules/tutorials/employees_helper.php'; include 'code.php'; ?>
</section>

<section id="View" class="editorbox mt-5 mb-5">
  <h5>View</h5>
  <?php $file_path = 'application/modules/tutorials/views/employees/form.php'; include 'code.php'; ?>
  <?php $file_path = 'application/modules/tutorials/views/employees/view.php'; include 'code.php'; ?>
</section>