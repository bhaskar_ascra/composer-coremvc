<h2>01 - Listing Page with Filters</h2>
<p>
  1. Single table listing<br />
  2. Sort - Ascending / Descending<br />
  3. Filter - Single Select Static Dropdown<br />
  4. Filter - Single Select Dynamic Dropdown<br />
  5. Filter - Multi-select Static Dropdown<br />
  6. Filter - Multi-select Dynamic Dropdown<br />
  7. Filter - Numeric Range Select<br />
  8. Filter - Date Filter - Single<br />
  9. Filter - Date Filter - Range<br />
  10. Filter - Autocomplete Dynamic<br />
  11. Filter - Like Search<br />
  12. Alias field<br />
  13. Export <br />
  14. Select Column <br />
  15. Arrange Column
</p>
<section id="Migrations" class="editorbox mt-5 mb-5">
  <h4>Migrations</h4>
  <p>Run these queries</p>
  <?php $file_path = 'application/modules/tutorials/migrations/20191025122035_create_table_industries.php'; include 'code.php'; ?>
</section>

<section id="Controller" class="editorbox mt-5 mb-5">
  <h5>Controller</h5>
  <?php $file_path = 'application/modules/tutorials/controllers/Industries.php'; include 'code.php'; ?>
</section>

<section id="Model" class="editorbox mt-5 mb-5">
  <h5>Model</h5>
  <?php $file_path = 'application/modules/tutorials/models/Industry_model.php'; include 'code.php'; ?>
</section>

<section id="Helper" class="editorbox mt-5 mb-5">
  <h5>Helper</h5>
  <?php $file_path = 'application/helpers/modules/tutorials/industries_helper.php'; include 'code.php'; ?>
</section>