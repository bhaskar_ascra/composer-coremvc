<h1>04 - One To Many Associations</h1>
<p>
  1. Create<br \>
  2. Edit<br \>
  3. View<br \>
  4. Associations Validations<br \>
  5. Adding Multiple Rows with JS<br \> 
  6. Delete Row - ajax (SOFT)<br \>
</p>
<section id="Migrations" class="editorbox mt-5 mb-5">
  <h5>Migrations</h5>
  <p>Run these queries</p>
  <?php $file_path = 'application/modules/tutorials/migrations/20191025123537_create_table_transactions.php'; 
    include 'code.php'; ?>
  <?php $file_path = 'application/modules/tutorials/migrations/20191025123731_create_table_transaction_details.php'; 
    include 'code.php'; ?>
  <?php $file_path = 'application/modules/tutorials/migrations/20191025161502_insert_into_transactions.php'; 
    include 'code.php'; ?>  
  <?php $file_path = 'application/modules/tutorials/migrations/20191025162102_insert_into_transaction_details.php'; 
    include 'code.php'; ?>    
</section>

<section id="Controller" class="editorbox mt-5 mb-5">
  <h5>Controller</h5>
  <?php $file_path = 'application/modules/tutorials/controllers/Transactions.php'; include 'code.php'; ?>
</section>

<section id="Model" class="editorbox mt-5 mb-5">
  <h5>Model</h5>
  <?php $file_path = 'application/modules/tutorials/models/Transaction_model.php'; include 'code.php'; ?>
  <?php $file_path = 'application/modules/tutorials/models/Transaction_detail_model.php'; include 'code.php'; ?>
</section>

<section id="Helper" class="editorbox mt-5 mb-5">
  <h5>Helper</h5>
  <?php $file_path = 'application/helpers/modules/tutorials/transactions_helper.php'; include 'code.php'; ?>
</section>

<section id="View" class="editorbox mt-5 mb-5">
  <h5>View</h5>
  <?php $file_path = 'application/modules/tutorials/views/transactions/form.php'; include 'code.php'; ?>
  <?php $file_path = 'application/modules/tutorials/views/transactions/subform.php'; include 'code.php'; ?>
  <?php $file_path = 'application/modules/tutorials/views/transactions/view.php'; include 'code.php'; ?>
  <?php $file_path = 'application/modules/tutorials/views/transaction_details/subform.php'; include 'code.php'; ?>
  <?php $file_path = 'application/modules/tutorials/views/transaction_details/subview.php'; include 'code.php'; ?>
</section>

<section id="JS" class="editorbox mt-5 mb-5">
  <h5>JS</h5>
  <?php $file_path = 'assets/theme/js/transactions/transaction_details.js'; include 'code.php'; ?>
</section>