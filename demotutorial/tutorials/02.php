<h1>02 - Create, Edit and View - Single Table </h1>
<p>
  1. Create - Using HTTP<br />
  2. Create - Using Ajax<br />
  3. Edit - Using HTTP<br \>
  4. View - Using HTTP<br \>
  5. Edit form using ajax<br \>
  6. View using ajax<br \>
  7. Standard Validations<br \>
  8. Unique Validations<br \>
  9. All type of form fields<br \>
    - Text<br \>
    - Dropdown<br \>
    - Time<br \>
    - Date<br \>
    - Textarea<br \>
    - Checkbox<br \>
    - Radio<br \>
  10. Multiple Actions JS, AJAX POST, AJAX GET, HTTP<br \>
</p> 
<section id="Migrations" class="editorbox mt-5 mb-5">
  <h4>Migrations</h4>
  <p>Run these queries</p>
  <?php $file_path = 'application/modules/tutorials/migrations/20191025123203_create_table_occupations.php'; 
    include 'code.php';?>
  <?php $file_path = 'application/modules/tutorials/migrations/20191025155441_insert_into_occupations.php';
    include 'code.php';?>
</section>

<section id="Controller" class="editorbox mt-5 mb-5">
  <h5>Controller</h5>
  <?php $file_path = 'application/modules/tutorials/controllers/Occupations.php'; include 'code.php'; ?>
</section>

<section id="Model" class="editorbox mt-5 mb-5">
  <h5>Model</h5>
  <?php $file_path = 'application/modules/tutorials/models/Occupation_model.php'; include 'code.php'; ?>
</section>

<section id="Helper" class="editorbox mt-5 mb-5">
  <h5>Helper</h5>
  <?php $file_path = 'application/helpers/modules/tutorials/occupations_helper.php'; include 'code.php'; ?>
</section>

<section id="View" class="editorbox mt-5 mb-5">
  <h5>View</h5>
  <?php $file_path = 'application/modules/tutorials/views/occupations/form.php'; include 'code.php'; ?>
  <?php $file_path = 'application/modules/tutorials/views/occupations/view.php'; include 'code.php'; ?>
  <?php $file_path = 'application/modules/tutorials/views/occupations/table_header.php'; include 'code.php'; ?>
</section>