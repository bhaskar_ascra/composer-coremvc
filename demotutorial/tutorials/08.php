<h1>08 - Association Dropdowns</h1>
<p>
  1. Get state on change of country <br \>
  2. Get city on change of state<br \>
</p>
<section id="Database" class="editorbox mt-5 mb-5">
  <h5>Migrations</h5>
  <p>Run these queries</p>
  <?php $file_path = 'application/modules/tutorials/migrations/20191025123920_create_table_vendors.php'; include 'code.php'; ?>
  <?php $file_path = 'application/modules/tutorials/migrations/20191025124047_create_table_countries.php'; include 'code.php'; ?>
  <?php $file_path = 'application/modules/tutorials/migrations/20191025124215_create_table_states.php'; include 'code.php'; ?>
  <?php $file_path = 'application/modules/tutorials/migrations/20191025124334_create_table_cities.php'; include 'code.php'; ?>
  <?php $file_path = 'application/modules/tutorials/migrations/20191025162401_insert_into_vendors.php'; include 'code.php'; ?>
</section>

<section id="Controller" class="editorbox mt-5 mb-5">
  <h5>Controller</h5>
  <?php $file_path = 'application/modules/tutorials/controllers/Vendors.php'; include 'code.php'; ?>
  <?php $file_path = 'application/modules/tutorials/controllers/States.php'; include 'code.php'; ?>
  <?php $file_path = 'application/modules/tutorials/controllers/Cities.php'; include 'code.php'; ?>
</section>

<section id="Model" class="editorbox mt-5 mb-5">
  <h5>Model</h5>
  <?php $file_path = 'application/modules/tutorials/models/Vendor_model.php'; include 'code.php'; ?>
  <?php $file_path = 'application/modules/tutorials/models/Country_model.php'; include 'code.php'; ?>
  <?php $file_path = 'application/modules/tutorials/models/State_model.php'; include 'code.php'; ?>
  <?php $file_path = 'application/modules/tutorials/models/City_model.php'; include 'code.php'; ?>
</section>

<section id="JS" class="editorbox mt-5 mb-5">
  <h5>JS</h5>
    <?php $file_path = 'assets/theme/js/vendors/country_state_city.js'; include 'code.php'; ?>

<section id="Helper" class="editorbox mt-5 mb-5">
  <h5>Helper</h5>
  <?php $file_path = 'application/helpers/modules/tutorials/vendors_helper.php'; include 'code.php'; ?>
</section>

<section id="View" class="editorbox mt-5 mb-5">
  <h5>View</h5>
  <?php $file_path = 'application/modules/tutorials/views/vendors/form.php'; include 'code.php'; ?>
</section>