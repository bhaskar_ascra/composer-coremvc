<h1>06 - Upload Images And Documents</h1>
<p>
  1. Upload Document<br \>
  2. Upload Image<br \>
  3. Document Validation<br \>
  4. Image Validation<br \>
</p>
<section id="Database" class="editorbox mt-5 mb-5">
  <h5>Migrations</h5>
  <p>Run these queries</p>
  <?php $file_path = 'application/modules/tutorials/migrations/20191025122426_create_table_my_documents_and_images.php'; 
    include 'code.php'; ?>
</section>

<section id="Controller" class="editorbox mt-5 mb-5">
  <h5>Controller</h5>
  <?php $file_path = 'application/modules/tutorials/controllers/My_documents_and_images.php'; include 'code.php'; ?>
</section>

<section id="Model" class="editorbox mt-5 mb-5">
  <h5>Model</h5>
  <?php $file_path = 'application/modules/tutorials/models/My_documents_and_image_model.php'; include 'code.php'; ?>
</section>

<section id="Helper" class="editorbox mt-5 mb-5">
  <h5>Helper</h5>
  <?php $file_path = 'application/helpers/modules/tutorials/my_documents_and_images_helper.php'; include 'code.php'; ?>
</section>

<section id="View" class="editorbox mt-5 mb-5">
  <h5>View</h5>
  <?php $file_path = 'application/modules/tutorials/views/my_documents_and_images/form.php'; include 'code.php'; ?>
  <?php $file_path = 'application/modules/tutorials/views/my_documents_and_images/view.php'; include 'code.php'; ?>
</section>