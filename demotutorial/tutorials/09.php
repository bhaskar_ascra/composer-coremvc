<h1>09 - Association Dropdowns - Static - JS</h1>
<p>
  1. Get state on change of country <br \>
  2. Get city on change of state<br \>
</p>
<section id="Database" class="editorbox mt-5 mb-5">
  <h5>Migrations</h5>
  <p>Run these queries</p>
  <?php $file_path = 'application/modules/tutorials/migrations/20191101125326_create_table_country_cities.php'; include 'code.php'; ?>
</section>

<section id="Controller" class="editorbox mt-5 mb-5">
  <h5>Controller</h5>
  <?php $file_path = 'application/modules/tutorials/controllers/Country_state_cities.php'; include 'code.php'; ?>
</section>

<section id="Model" class="editorbox mt-5 mb-5">
  <h5>Model</h5>
  <?php $file_path = 'application/modules/tutorials/models/Country_state_city_model.php'; include 'code.php'; ?>
</section>

<section id="JS" class="editorbox mt-5 mb-5">
  <h5>JS</h5>
    <?php $file_path = 'assets/theme/js/country_state_cities/country_state_city.js'; include 'code.php'; ?>

<section id="Helper" class="editorbox mt-5 mb-5">
  <h5>Helper</h5>
  <?php $file_path = 'application/helpers/modules/tutorials/country_state_cities_helper.php'; include 'code.php'; ?>
</section>

<section id="View" class="editorbox mt-5 mb-5">
  <h5>View</h5>
  <?php $file_path = 'application/modules/tutorials/views/country_state_cities/form.php'; include 'code.php'; ?>
</section>