<?php 
  include 'head.php'; 
  $PROJECT_PATH = getcwd().'/../';
?>
	
<div class="sidebar" style="display: none;">
  
</div>

<div class="content pt-2">
		<div id="page">
      <?php if(isset($_GET['tutorials'])):?>
        <?php include 'tutorials/'.$_GET['tutorials']; ?>
      <?php else: ?>
  			<div class="links">
          <a class="active" href="?tutorials=01.php">01-Listing-page-with-filters</a><br>
          <a class="active" href="?tutorials=02.php">02-Create-Edit-View-Single-Table</a><br>
          <a class="active" href="?tutorials=04.php">04-One-To-Many-Associations</a><br>
          <a class="active" href="?tutorials=06.php">06-Upload-Images-And-Documents</a><br>
          <a class="active" href="?tutorials=07.php">07-Listing-With-Joins</a><br>
  			  <a class="active" href="?tutorials=08.php">08-Association-Dropdowns-Ajax</a><br>
  			  <a href="?tutorials=00.php">Demo</a><br>
  			  <a href="#csscode">CSS Code</a><br>
  			</div>
      <?php endif; ?>
	</div>
</div>
<?php include 'footer.php';?>