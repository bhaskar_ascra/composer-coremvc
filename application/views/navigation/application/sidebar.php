<?php
  $controller = $this->router->directory.$this->router->class;
  $sessionData = $this->session->userdata();
?>

<aside class="sidenavbar sidenavbar_js expand">
  <div class="sidenav_scroll_js sidenav_scroll">
    <!-- <?php if(isset($_SESSION) && !empty($_SESSION)):?> -->
    <!-- <ul> 
      <li>
        <a href="<?= ADMIN_PATH ?>users/logout">
          <span class="icon"><i class="fal fa-sign-out-alt"></i></span>
          <span class="hide-menu">Logout</span>
        </a> 
      </li>
    </ul>
    <?php endif;?> -->
    <ul> 
      <!-- <li class="active">
        <?php if(isset($_SESSION['is_sp']) && $_SESSION['is_sp'] == 1):?>
        <a href="<?= ADMIN_PATH ?>dashboards/Sp_dashboards">
          <span class="icon"><i class="fal fa-tachometer-alt"></i></span>
          <span class="hide-menu">SP Dashboard</span>
        </a>
        <ul> 
          <?php 
            $this->load->view('navigation/application/sidebar_item', 
              array('url' => ADMIN_PATH.'dashboards/sp_contracting_in_my_professionals',
              'active' => ($controller=='professionals') ? 'active' : '',
              'title' => 'Contracting IN My Professional',             
            ));
            $this->load->view('navigation/application/sidebar_item', 
              array('url' => ADMIN_PATH.'dashboards/sp_contracting_in_all_professionals',
              'active' => ($controller=='professionals') ? 'active' : '',
              'title' => 'Contracting IN All Professional',             
            ));
            $this->load->view('navigation/application/sidebar_item', 
              array('url' => ADMIN_PATH.'dashboards/sp_contracting_out_my_professionals',
              'active' => ($controller=='professionals') ? 'active' : '',
              'title' => 'Contracting Out My Professional',             
            ));
            $this->load->view('navigation/application/sidebar_item', 
              array('url' => ADMIN_PATH.'dashboards/sp_contracting_out_all_professionals',
              'active' => ($controller=='professionals') ? 'active' : '',
              'title' => 'Contracting Out All Professional',             
            ));
          ?>
        </ul>
        <?php else:?>
        <a href="<?= ADMIN_PATH ?>dashboards/my_dashboards">
          <span class="icon"><i class="fal fa-tachometer-alt"></i></span>
          <span class="hide-menu">My Dashboard</span>
        </a>
      <?php endif;?>
      </li> -->
      <li class="submenu submenu_js">
        <a href="javascript:void(0);" class="dropicon fa-chevron-right">
          <span class="icon">
            <i class="fal fa-user-tie"></i>
          </span>
          <span>Tutorials</span>
        </a>
        <ul> 
          <?php if(isset($_SESSION['is_sp']) && $_SESSION['is_sp'] == 1):else: 
            $this->load->view('navigation/application/sidebar_item', 
              array('url' => ADMIN_PATH.'tutorials/industries',
              'active' => ($controller=='industries') ? 'active' : '',
              'title' => 'Tutorial 01 - Industries',             
            ));
            $this->load->view('navigation/application/sidebar_item', 
              array('url' => ADMIN_PATH.'tutorials/occupations',
              'active' => ($controller=='occupations') ? 'active' : '',
              'title' => 'Tutorial 02 - Occupations',             
            ));
          endif;
            $this->load->view('navigation/application/sidebar_item', 
              array('url' => ADMIN_PATH.'tutorials/transactions',
              'active' => ($controller=='transactions') ? 'active' : '',
              'title' => 'Tutorial 04 - Transactions & Transactions Details',             
            ));
            $this->load->view('navigation/application/sidebar_item', 
              array('url' => ADMIN_PATH.'tutorials/my_documents_and_images',
              'active' => ($controller=='my_documents_and_images') ? 'active' : '',
              'title' => 'Tutorial 06 - Images And Documents',             
            )); 
            $this->load->view('navigation/application/sidebar_item', 
              array('url' => ADMIN_PATH.'tutorials/employees',
              'active' => ($controller=='employees') ? 'active' : '',
              'title' => 'Tutorial 07 - Employees',             
            ));
            $this->load->view('navigation/application/sidebar_item', 
              array('url' => ADMIN_PATH.'tutorials/vendors',
              'active' => ($controller=='vendors') ? 'active' : '',
              'title' => 'Tutorial 08 - Vendors',             
            ));
            /*$this->load->view('navigation/application/sidebar_item', 
              array('url' => ADMIN_PATH.'professionals/professional_shared',
              'active' => ($controller=='professionals') ? 'active' : '',
              'title' => 'Shared',             
            ));*/
            /*$this->load->view('navigation/application/sidebar_item', 
              array('url' => ADMIN_PATH.'professionals/professional_searches',
              'active' => ($controller=='professionals') ? 'active' : '',
              'title' => 'Searches',             
            ));
            $this->load->view('navigation/application/sidebar_item', 
              array('url' => ADMIN_PATH.'professionals/professional_work_status',
              'active' => ($controller=='professionals') ? 'active' : '',
              'title' => 'Work Status',             
            ));
            $this->load->view('navigation/application/sidebar_item', 
              array('url' => ADMIN_PATH.'professionals/professional_references',
              'active' => ($controller=='professionals') ? 'active' : '',
              'title' => 'Professional references',             
            ));*/
          ?>
        </ul>
      </li>
       <?php //if(isset($_SESSION['is_sp']) &&  $_SESSION['is_sp'] == 1):else:?>
        <!-- <li class="submenu submenu_js">
          <a href="javascript:void(0);" class="dropicon fa-chevron-right">
            <span class="icon">
              <i class="fal fa-briefcase"></i>
            </span>
            <span>Businesses</span>
          </a>
          <ul>
            <?php $this->load->view('navigation/application/sidebar_item', 
              array('url' => ADMIN_PATH.'dashboards/customers',
              'active' => ($controller=='customers') ? 'active' : '',
              'title' => 'Business Dashboard',
            ));
            $this->load->view('navigation/application/sidebar_item', 
              array('url' => ADMIN_PATH.'dashboards/customer_service_partners',
              'active' => ($controller=='customers') ? 'active' : '',
              'title' => 'Service Partner<br>Admin Dashboard',
            ));
            $this->load->view('navigation/application/sidebar_item', 
              array('url' => ADMIN_PATH.'dashboards/customer_dashboards',
              'active' => ($controller=='customers') ? 'active' : '',
              'title' => 'Customer Dashboard',
            ));
            $this->load->view('navigation/application/sidebar_item', 
              array('url' => ADMIN_PATH.'customers',
              'active' => ($controller=='customers') ? 'active' : '',
              'title' => 'All Businesses',
            )); ?>
          </ul>
        </li> --> 
        <?php //endif;?>
      <!-- <li class="submenu submenu_js">
        <a href="javascript:void(0);" class="dropicon fa-chevron-right">
          <span class="icon">
            <i class="fal fa-building"></i>
          </span>
          <span>Works</span>
        </a>
        <ul>
           <?php if(isset($_SESSION['is_sp']) &&  $_SESSION['is_sp'] == 1):
            $this->load->view('navigation/application/sidebar_item', 
              array('url' => ADMIN_PATH.'dashboards/works',
              'active' => ($controller=='works') ? 'active' : '',
              'title' => 'Work Dashboard',
            ));
          else:
            $this->load->view('navigation/application/sidebar_item', 
              array('url' => ADMIN_PATH.'dashboards/hourly_contracts',
              'active' => ($controller=='works') ? 'active' : '',
              'title' => 'Hourly contract Dashboard',
            ));
            $this->load->view('navigation/application/sidebar_item', 
              array('url' => ADMIN_PATH.'dashboards/direct_jobs',
              'active' => ($controller=='works') ? 'active' : '',
              'title' => 'Direct Job Dashboard',
            )); 
            $this->load->view('navigation/application/sidebar_item', 
              array('url' => ADMIN_PATH.'dashboards/hard_quotes',
              'active' => ($controller=='works') ? 'active' : '',
              'title' => 'Hard Quote Dashboard',
            ));
            $this->load->view('navigation/application/sidebar_item', 
              array('url' => ADMIN_PATH.'dashboards/contract_to_hires',
              'active' => ($controller=='works') ? 'active' : '',
              'title' => 'Contract to Hire Dashboard',
            ));
            endif;
            $this->load->view('navigation/application/sidebar_item', 
              array('url' => ADMIN_PATH.'works/work_requests',
              'active' => ($controller=='works') ? 'active' : '',
              'title' => 'All Work',
            )); 
            /*$this->load->view('navigation/application/sidebar_item', 
              array('url' => ADMIN_PATH.'works/work_shared',
              'active' => ($controller=='works') ? 'active' : '',
              'title' => 'Shared',
            ));*/
            $this->load->view('navigation/application/sidebar_item', 
              array('url' => ADMIN_PATH.'works/work_searches',
              'active' => ($controller=='works') ? 'active' : '',
              'title' => 'Searches',
            ));
            $this->load->view('navigation/application/sidebar_item', 
              array('url' => ADMIN_PATH.'works/work_applications',
              'active' => ($controller=='works') ? 'active' : '',
              'title' => 'Work Applications',
            ));
            $this->load->view('navigation/application/sidebar_item', 
              array('url' => ADMIN_PATH.'works/work_reapply_requests',
              'active' => ($controller=='works') ? 'active' : '',
              'title' => 'Re-apply request',
            ));
          ?>
        </ul>
      </li> -->
      <!-- <li class="submenu submenu_js">
        <a href="javascript:void(0);" class="dropicon fa-chevron-right">
          <span class="icon">
            <i class="fal fa-user-cog"></i>
          </span>
          <span>Masters</span>
        </a>
        <ul>
          <?php $this->load->view('navigation/application/sidebar_item', 
            array('url' => ADMIN_PATH.'masters/occupations',
            'active' => ($controller=='masters') ? 'active' : '',
            'title' => 'Occupations',
          ));
          $this->load->view('navigation/application/sidebar_item', 
            array('url' => ADMIN_PATH.'masters/industries',
            'active' => ($controller=='masters') ? 'active' : '',
            'title' => 'Industries',
          )); 
          $this->load->view('navigation/application/sidebar_item', 
            array('url' => ADMIN_PATH.'masters/manufacturers',
            'active' => ($controller=='masters') ? 'active' : '',
            'title' => 'Manufacturers',
          ));
          $this->load->view('navigation/application/sidebar_item', 
            array('url' => ADMIN_PATH.'masters/manufacturermodels',
            'active' => ($controller=='masters') ? 'active' : '',
            'title' => 'Manufacturers models',
          ));
          $this->load->view('navigation/application/sidebar_item', 
            array('url' => ADMIN_PATH.'masters/tools',
            'active' => ($controller=='masters') ? 'active' : '',
            'title' => 'Tools, machines &<br>equipment competencies',
          )); 
          $this->load->view('navigation/application/sidebar_item', 
            array('url' => ADMIN_PATH.'masters/software_licenses',
            'active' => ($controller=='masters') ? 'active' : '',
            'title' => 'Software liceneces',
          ));
          $this->load->view('navigation/application/sidebar_item', 
            array('url' => ADMIN_PATH.'masters/applications',
            'active' => ($controller=='masters') ? 'active' : '',
            'title' => 'Applications',
          ));
          $this->load->view('navigation/application/sidebar_item', 
            array('url' => ADMIN_PATH.'masters/referrals',
            'active' => ($controller=='masters') ? 'active' : '',
            'title' => 'Referrals',
          ));
          $this->load->view('navigation/application/sidebar_item', 
            array('url' => ADMIN_PATH.'masters/companies',
            'active' => ($controller=='masters') ? 'active' : '',
            'title' => 'Companies',
          ));
          $this->load->view('navigation/application/sidebar_item', 
            array('url' => ADMIN_PATH.'masters/degrees',
            'active' => ($controller=='masters') ? 'active' : '',
            'title' => 'Degree',
          ));
          $this->load->view('navigation/application/sidebar_item', 
            array('url' => ADMIN_PATH.'masters/schools',
            'active' => ($controller=='masters') ? 'active' : '',
            'title' => 'School',
          ));
          $this->load->view('navigation/application/sidebar_item', 
            array('url' => ADMIN_PATH.'masters/specializations',
            'active' => ($controller=='masters') ? 'active' : '',
            'title' => 'Specializations',
          ));
          $this->load->view('navigation/application/sidebar_item', 
            array('url' => ADMIN_PATH.'masters/professional_competencies',
            'active' => ($controller=='masters') ? 'active' : '',
            'title' => 'Professional competencies',
          ));
          $this->load->view('navigation/application/sidebar_item', 
            array('url' => ADMIN_PATH.'masters/skilled_trades',
            'active' => ($controller=='masters') ? 'active' : '',
            'title' => 'Skilled Trades',
          ));
          /*$this->load->view('navigation/application/sidebar_item', 
            array('url' => ADMIN_PATH.'masters/users',
            'active' => ($controller=='masters') ? 'active' : '',
            'title' => 'Users',
          ));*/
            ?>
        </ul>
      </li> -->
      
    <!--  <li class="submenu submenu_js">
        <a href="javascript:void(0);" class="dropicon fa-chevron-right">
          <span class="icon">
            <i class="fal fa-file-spreadsheet"></i>
          </span>
          <span>Timesheets</span>
        </a>
        <ul> 
          <?php $this->load->view('navigation/application/sidebar_item', 
            array('url' => ADMIN_PATH.'dashboards/timesheets',
            'active' => ($controller=='accounts') ? 'active' : '',
            'title' => 'Timesheets Dashboard',
          ));
          $this->load->view('navigation/application/sidebar_item', 
            array('url' => ADMIN_PATH.'accounts/due_timesheets',
            'active' => ($controller=='accounts') ? 'active' : '',
            'title' => 'Due Timesheets',
          ));
          $this->load->view('navigation/application/sidebar_item', 
            array('url' => ADMIN_PATH.'accounts/timesheets',
            'active' => ($controller=='accounts') ? 'active' : '',
            'title' => 'All Timesheets',
          ));
           ?>                        
        </ul>
      </li> -->
      <!-- <li class="submenu submenu_js">
        <a href="javascript:void(0);" class="dropicon fa-chevron-right">
          <span class="icon">
            <i class="fal fa-money-check-alt"></i>
          </span>
          <span>Expenses</span>
        </a>
        <ul>                         
          <?php $this->load->view('navigation/application/sidebar_item', 
            array('url' => ADMIN_PATH.'dashboards/expenses',
            'active' => ($controller=='accounts') ? 'active' : '',
            'title' => 'Expenses Dashboard', 
          ));
          $this->load->view('navigation/application/sidebar_item', 
            array('url' => ADMIN_PATH.'accounts/due_expenses',
            'active' => ($controller=='accounts') ? 'active' : '',
            'title' => 'Due Expenses', 
          ));
          $this->load->view('navigation/application/sidebar_item', 
            array('url' => ADMIN_PATH.'accounts/expenses',
            'active' => ($controller=='accounts') ? 'active' : '',
            'title' => 'All Expenses', 
          ));
           ?>
        </ul>
      </li> -->
      <!-- <li class="submenu submenu_js">
        <a href="javascript:void(0);" class="dropicon fa-chevron-right">
          <span class="icon">
            <i class="fal fa-file-invoice-dollar"></i>
          </span>
          <span>Professionals Invoices</span>
        </a>
        <ul>      
          <?php $this->load->view('navigation/application/sidebar_item', 
            array('url' => ADMIN_PATH.'dashboards/professional_invoices',
            'active' => ($controller=='accounts') ? 'active' : '',
            'title' => 'Professionals Invoices Dashboard',
          ));
          $this->load->view('navigation/application/sidebar_item', 
            array('url' => ADMIN_PATH.'accounts/pending_timesheet_invoices',
            'active' => ($controller=='accounts') ? 'active' : '',
            'title' => 'Pending Timesheet',
          ));
          $this->load->view('navigation/application/sidebar_item', 
            array('url' => ADMIN_PATH.'accounts/pending_expense_invoices',
            'active' => ($controller=='accounts') ? 'active' : '',
            'title' => 'Pending Expense',
          ));
          $this->load->view('navigation/application/sidebar_item', 
            array('url' => ADMIN_PATH.'accounts/invoice_professionals_timesheet',
            'active' => ($controller=='accounts') ? 'active' : '',
            'title' => 'Ready Timesheet',
            ));
          $this->load->view('navigation/application/sidebar_item', 
            array('url' => ADMIN_PATH.'accounts/invoice_professionals_expense',
            'active' => ($controller=='accounts') ? 'active' : '',
            'title' => 'Ready Expense',
          )); ?>            
        </ul>
      </li> -->
      <!-- <?php if(isset($_SESSION['is_sp']) &&  $_SESSION['is_sp'] == 1):else:?>
      <li class="submenu submenu_js">
        <a href="javascript:void(0);" class="dropicon fa-chevron-right">
          <span class="icon">
            <i class="fal fa-file-invoice-dollar"></i>
          </span>
          <span>Businesses Invoices</span> 
        </a>  
        <ul>
          <?php $this->load->view('navigation/application/sidebar_item', 
            array('url' => ADMIN_PATH.'dashboards/customer_invoices',
            'active' => ($controller=='accounts') ? 'active' : '',
            'title' => 'Businesses Invoices Dashboard',
          ));
          $this->load->view('navigation/application/sidebar_item', 
            array('url' => ADMIN_PATH.'accounts/pending_customer_timesheet_invoices',
            'active' => ($controller=='accounts') ? 'active' : '',
            'title' => 'Pending Timesheet',
          ));
          $this->load->view('navigation/application/sidebar_item', 
            array('url' => ADMIN_PATH.'accounts/pending_customer_expense_invoices',
            'active' => ($controller=='accounts') ? 'active' : '',
            'title' => 'Pending Expense',
          ));
          $this->load->view('navigation/application/sidebar_item', 
            array('url' => ADMIN_PATH.'accounts/invoice_customers_timesheet',
            'active' => ($controller=='accounts') ? 'active' : '',
            'title' => 'Ready Timesheet',
          ));
          $this->load->view('navigation/application/sidebar_item', 
            array('url' => ADMIN_PATH.'accounts/invoice_customers_expense',
            'active' => ($controller=='accounts') ? 'active' : '',
            'title' => 'Ready Expense',
          )); ?>          
        </ul>     
      </li>
      <?php endif;?>
      <li class="submenu submenu_js">
        <a href="javascript:void(0);" class="dropicon fa-chevron-right">
          <span class="icon">
            <i class="fal fa-money-bill-alt"></i>
          </span>
          <span>Financial dashboards</span> 
        </a>  
        <ul>
          <?php 
          if(isset($_SESSION['is_sp']) &&  $_SESSION['is_sp'] == 1):else:
            $this->load->view('navigation/application/sidebar_item', 
              array('url' => ADMIN_PATH.'dashboards/financials',
              'active' => ($controller=='dashboards') ? 'active' : '',
              'title' => 'Admin Financial Dashboard',
            ));
          endif;
          $this->load->view('navigation/application/sidebar_item', 
            array('url' => ADMIN_PATH.'dashboards/professional_financials',
            'active' => ($controller=='dashboards') ? 'active' : '',
            'title' => 'Professional Financial Dashboard',
          ));
          if(isset($_SESSION['is_sp']) &&  $_SESSION['is_sp'] == 1):else:
            $this->load->view('navigation/application/sidebar_item', 
              array('url' => ADMIN_PATH.'dashboards/customer_financials',
              'active' => ($controller=='dashboards') ? 'active' : '',
              'title' => 'Business Financial Dashboard',
            ));
          endif;?>          
        </ul>     
      </li>
      
      <li class="submenu submenu_js">
        <a href="javascript:void(0);" class="dropicon fa-chevron-right">
          <span class="icon">
            <i class="fal fa-comment-alt-edit"></i>
          </span>
          <span>Reviews</span> 
        </a>  
        <ul>
          <?php $this->load->view('navigation/application/sidebar_item', 
            array('url' => ADMIN_PATH.'professionals/professional_reviews',
            'active' => ($controller=='professionals') ? 'active' : '',
            'title' => 'Professionals',
          ));
          $this->load->view('navigation/application/sidebar_item', 
            array('url' => ADMIN_PATH.'customers/customer_reviews',
            'active' => ($controller=='reviews') ? 'active' : '',
            'title' => 'Businesses',
          ));?>           
        </ul>     
      </li>
      <li class="submenu submenu_js">
        <a href="<?= ADMIN_PATH ?>home/blogs">
          <span class="icon">
            <i class="fal fa-blog"></i>
          </span>
          <span>Blog</span>
        </a>
      </li>
      <li class="submenu submenu_js">
        <a href="javascript:void(0);">
          <span class="icon">
            <i class="fal fa-cogs"></i>
          </span>
          <span>Setting</span>
        </a>
      </li>
      <li class="submenu submenu_js">
        <a href="javascript:void(0);" class="dropicon fa-chevron-right">
          <span class="icon">
            <i class="fal fa-hashtag"></i>
          </span>
          <span>Social's Media</span>
        </a>
        <ul>                         
          <?php $this->load->view('navigation/application/sidebar_item', 
            array('url' => ADMIN_PATH."socials_media/Linkedin",
            'active' => ($controller=='socials_media') ? 'active' : '',
            'title' => 'LinkedIn', 
          ));
          $this->load->view('navigation/application/sidebar_item', 
            array('url' => ADMIN_PATH.'socials_media/Linkedin_log',
            'active' => ($controller=='socials_media') ? 'active' : '',
            'title' => 'LinkedIn Log', 
          )); ?>
        </ul>
      </li>
      <li class="submenu submenu_js">
        <a href="<?= ADMIN_PATH ?>home/help_texts" class="">
          <span class="icon">
            <i class="fal fa-question-circle"></i>
          </span>
          <span>Help Text</span> 
        </a>  
      </li>
      <li class="submenu submenu_js">
        <a href="<?= ADMIN_PATH ?>home/cover_photos" class="">
          <span class="icon">
            <i class="fal fa-image"></i>
          </span>
          <span>Cover Photo</span> 
        </a>  
      </li>
      <li class="submenu submenu_js">
        <a href="<?= ADMIN_PATH ?>home/video_libraries" class="">
          <span class="icon">
            <i class="fal fa-file-video"></i>
          </span>
          <span>Video Library</span> 
        </a>  
      </li>
      <li class="submenu submenu_js">
        <a href="<?= ADMIN_PATH ?>home/image_libraries" class="">
          <span class="icon">
            <i class="fal fa-images"></i>
          </span>
          <span>Gallery</span> 
        </a>  
      </li>
      <li class="submenu submenu_js">
        <a href="<?= ADMIN_PATH ?>home/search_banners" class="">
          <span class="icon">
            <i class="fal fa-image"></i>
          </span>
          <span>Search Banner</span> 
        </a>  
      </li>
      <li class="submenu submenu_js">
        <a href="javascript:void(0);" class="dropicon fa-chevron-right">
          <span class="icon">
            <i class="fal fa-bullhorn"></i>
          </span>
          <span>Communication</span>
        </a>
        <ul>
        <?php $this->load->view('navigation/application/sidebar_item', 
                             array('url' => ADMIN_PATH."communications/configurations/create",
                                        'active' => ($controller=='communications') ? 'active' : '',
                                        'title' => 'Configuration', 
                                        )
                                    ); ?>
            <?php $this->load->view('navigation/application/sidebar_item', 
                             array('url' => ADMIN_PATH."communications/templates",
                                       'active' => ($controller=='communications') ? 'active' : '',
                                       'title' => 'Templates', 
                                        )
                                    ); ?>           
        <?php $this->load->view('navigation/application/sidebar_item', 
                             array('url' => ADMIN_PATH."communications/unsubscribes",
                                        'active' => ($controller=='communications') ? 'active' : '',
                                       'title' => 'Unsubscribes', 
                                        )
                                    ); ?>  
            <?php $this->load->view('navigation/application/sidebar_item', 
                             array('url' => ADMIN_PATH."communications/bounced_emails",
                                       'active' => ($controller=='communications') ? 'active' : '',
                                       'title' => 'Bounced Email', 
                                        )
                                    ); ?>                             
          <?php $this->load->view('navigation/application/sidebar_item', 
                             array('url' => ADMIN_PATH."communications/email_logs",
                                        'active' => ($controller=='communications') ? 'active' : '',
                                        'title' => 'Email Logs', 
                                        )
                                    ); ?>
            <?php $this->load->view('navigation/application/sidebar_item', 
                                   array('url' => ADMIN_PATH.'communications/sms_logs"',
                                           'active' => ($controller=='communications') ? 'active' : '',
                                            'title' => 'Sms Logs', 
                                        )
                                    ); ?>
            <?php $this->load->view('navigation/application/sidebar_item', 
                                   array('url' => ADMIN_PATH.'communications/pushnotification_logs"',
                                           'active' => ($controller=='communications') ? 'active' : '',
                                            'title' => 'Push Notification Logs', 
                                        )
                                    ); ?>
            <?php $this->load->view('navigation/application/sidebar_item', 
                                   array('url' => ADMIN_PATH.'communications/website_inquiries"',
                                           'active' => ($controller=='communications') ? 'active' : '',
                                            'title' => 'Website Inquiries', 
                                        )
                                    ); ?> -->
      </ul>
    </li> 
    </ul>   
  </div>

</aside>