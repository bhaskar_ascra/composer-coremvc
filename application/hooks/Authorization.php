<?php
class Authorization {
  function check_url_authorization(){
    $ci =& get_instance();
    $ci->load->model(array('users/Users_user_role_model','users/User_role_permission_model'));
    $url = $ci->router->module.'/'.$ci->router->class.'/'.$ci->router->method;
    $action = $ci->router->method;
    if (!in_array($url, available_urls_before_login())) {
      $user_id = $_SESSION['user_id'];
      $user_role_ids_array = $_SESSION['user_role_ids'];

      $role_permissions = ''; 
      if (!empty($user_role_ids_array)) {
        if ($action == 'update')
          $action = 'edit';
        elseif ($action == 'store')
          $action = 'create';

        $role_permissions = '';
        if ($action=='index' || $action=='create' || $action=='edit' || $action=='view' || $action=='delete'){

          $user_role_ids = implode(', ',$user_role_ids_array);
          $role_permissions = @$ci->User_role_permission_model->find('*',
                                   array(/*'controller_name' => $ci->router->module,
                                     $action => 1,*/ 'where_in' => array('user_role_id' => $user_role_ids)
                                                        ));
        }
      }
      if (empty($role_permissions) 
          && !($action == 'index')) {
        if ($this->is_ajax()) {
          echo ('Access Denied'); 
          die();
        } 
        echo ('Access Denied'); 
        die();
      }
    }
  }
  private function is_ajax() {
    $is_ajax = (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');
    return $is_ajax;
  }
}

?>