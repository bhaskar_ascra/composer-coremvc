<?php
class Authentication {
  function check_authentication() {
    $ci =& get_instance();
    $ci->load->helper('modules/authentication/authentication_helper');
    $ci->load->model('users/user_model');
    $ci->load->model('users/users_user_role_model');
    $url = $ci->router->module.'/'.$ci->router->class.'/'.$ci->router->method;
    
    if(isset($_GET['access_token']) && !empty($_GET['access_token'])):
      $_SESSION = $ci->user_model->set_user_data_in_session(array("access_token" => $_GET['access_token']));
    endif;
    if(!isset($_SESSION['user_id'])
      && empty($_SESSION['user_id']) 
      && !in_array($url, available_urls_before_login())):
      if(isset($_SERVER['QUERY_STRING']) && !empty($_SERVER['QUERY_STRING']))
        $_SESSION['http_referer'] = $url.'?'.$_SERVER['QUERY_STRING'];
      else
        $_SESSION['http_referer'] = $url;
      redirect(base_url().'users/login/create');
    elseif(isset($_SESSION['user_id']) && !empty($_SESSION['user_id']) && in_array($url, available_urls_before_login())):
      redirect(base_url());
    endif;
  }
}
?>
