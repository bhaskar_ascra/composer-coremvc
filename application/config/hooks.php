<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| Hooks
| -------------------------------------------------------------------------
| This file lets you define "hooks" to extend CI without hacking the core
| files.  Please see the user guide for info:
|
|	https://codeigniter.com/user_guide/general/hooks.html
|
*/
$hook['post_controller_constructor'] = array(
  array(
    'class'    => 'Authentication',
    'function' => 'check_authentication',
    'filename' => 'Authentication.php',
    'filepath' => 'hooks',
    'params'   => ""
  ),
  array(
    'class'    => 'Authorization',
    'function' => 'check_url_authorization',
    'filename' => 'Authorization.php',
    'filepath' => 'hooks',
    'params'   => ""
  ),
); 
