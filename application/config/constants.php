<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE')  OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  OR define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ')                           OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE')  OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/

defined('EXIT_SUCCESS')        OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code

$root =(isset($_SERVER['HTTPS']) ? "https://" : "http://").$_SERVER['HTTP_HOST'];
$root .= str_replace(basename($_SERVER['SCRIPT_NAME']),'', $_SERVER['SCRIPT_NAME']);
define('ADMIN_PATH', $root);
define('SITE_PATH', $root);
// define('SITE_PATH', $config['base_url']);
define('CORE_PATH', $root.'assets/core/');
define('THEME_PATH', $root.'assets/theme/');
define('LOGIN_PATH', $root.'assets/login/');
define('ADMIN_LAYOUTS_PATH', $root.'assets/application/');
define('ADMIN_CSS_PATH',$root.'assets/css/');
define('ADMIN_FONT_PATH', $root.'assets/fonts/');
define('ADMIN_JS_PATH',$root.'assets/js/');
define('ADMIN_IMAGES_PATH',$root.'assets/application/images/');
define('ADMIN_PAGES_PATH',$root.'assets/css/pages/');
define('ADMIN_PLUGINS_PATH',$root.'assets/plugins/');
define('DEFAULT_IMAGE',$root.'assets/application/default/default.png');
define('COMMON_IMAGES_PATH',$root.'assets/application/images/common/');
define('FRONTEND_IMAGES_PATH',$root.'assets/application/images/frontend/');
define('BACKEND_IMAGES_PATH', $root.'assets/application/images/backend/');
define('THEME_COMMON_IMAGES_PATH',$root.'assets/theme/images/common/');
define('AWS', 'AWS');
define('LOCAL', 'local');
define('DOC', 'doc');
define('FOLDER', 'folder');
define('IMAGE', 'img');
define('PDF', 'pdf');

define('USER_SESS_KEY', 'user_session_key');
define('ADMIN_SESS_KEY', 'admin_session_key');
define('PAGES_MINIFY',false);
define('AWS_BUCKET_ACCESS_KEY', 'AKIAIW3CFXI5PRKRHR5Q');
define('AWS_BUCKET_SECRET_KEY', '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');
define('REGION', 'us-east-1');
define('UPLOAD_ON', 'aws');
define('S3_BUCKET', 'automationtalent-development');
define('MAX_SIZE', '10485760');

define('GOOGLE_CLIENT_ID', '279911217067-5dtvi9cktifoici4jb9v9mgu3bl42mi6.apps.googleusercontent.com');
define('GOOGLE_CLIENT_SECRET', 'Oxh1Ya2sYiD3MXUViJwxZ9Ft');
define('GOOGLE_REDIRECT_URL', 'http://localhost/coremvc/socials_media/Google/store');


define('LINKEDIN_CLIENT_ID', '86tbhfuo2asmej');
define('LINKEDIN_CLIENT_SECRET', 'rqSNrU9QtBqUtz1g');
define('LINKEDIN_REDIRECT_URL', 'http://localhost/coremvc/socials_media/Linkedin/store');

define('SMS_API_URL','https://api.twilio.com/2010-04-01/Accounts/AC3eac3be2422bceba52a88e6a5c3ef0b6/Messages.json');
define('SMS_API_FROM_NUMBER','+18435082948');
define('BULK_SMS_USERNAME', 'AC3eac3be2422bceba52a88e6a5c3ef0b6');
define('BULK_SMS_PASSWORD', '88ee6b2676dbe71ee7094e3052c8d417');
