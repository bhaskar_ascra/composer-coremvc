<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*version 1.1*/
trait Filter_trait  {
  private function getOrderHtml($heading) //order html
  {
    $order_by = "";
    if ($heading[2]) {
      $orderByData = array('heading'=>$heading[3],'orderData'=>$this->orderData,
                          'page_url'=>$this->page_url);
      $order_by = $this->load->view('layouts/application/list/table_sort',$orderByData,true);
    }
    return $order_by;
  }

  private function getFilterHtml($heading, $k) //get filter HTML
  {
    $filter_by = "";
    $current_url = base_url().$this->module;
    $search_param = $heading[1];
    $module = $this->module;
    $get_current_coulmn_status = $this->get_current_column_get_data();
    $selected_column = $get_current_coulmn_status['selected_column'];
    $ordered_columns = $get_current_coulmn_status['ordered_columns'];
    $dashboard_id = $get_current_coulmn_status['dashboard_id'];
    $query_string = $_SERVER['QUERY_STRING'];
    $where_data = array();
    parse_str($query_string,$_GET);
    $where_array = (isset($_GET['where'])?$_GET['where']:array());
    foreach ($where_array as $array_key => $array_value) {
      if(!empty($array_value))
        $where_data[$array_key] = $array_value;
    }
    $like_array = (isset($_GET['like'])?$_GET['like']:array());
    $where_set = array();
    foreach ($like_array as $like_key => $like_value) {
      $where_data[$like_key] = $like_value;
    }

    foreach ($where_data as $key => $where_value) {
      $where_set[str_replace(array('<=','>=','='), '', $key)] = $where_value;
    }

    $_SESSION['query_string_filter'] = $where_set;
    if ($heading[4]) {
      $filterHtml = array('k'=>$k,
                          'headingFunction'=>$this->headingFunction,
                          'search_url'=>$this->search_url,'current_url'=>$current_url,
                          'module'=>$module,'search_param'=>$search_param,
                          'query_string'=>basename($_SERVER['REQUEST_URI']),'dashboard_id'=>$dashboard_id);
       $filter_by = $this->load->view('layouts/application/list/table_filter',$filterHtml,TRUE);
    }
    return $filter_by;
  }

  private function filterHtml() {
    $html = '';
    $result['current_url'] = base_url().$this->module.'/'.$this->controller;
    if (!empty($this->getData)) :
        $result['param'] = $this->getData;
        $result['heading'] = $this->theadColumn;
        $html = $this->load->view('sys/search/view', $result, true);
    endif;
    return $html;
  }
}

?>