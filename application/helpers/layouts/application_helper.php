<?php 

function APPLICATION_CSS($type='application'){
	$css =  array(
		CORE_PATH().'plugins/bootstrap-4.3.1/css/bootstrap.min.css',
		CORE_PATH().'plugins/bootstrap-select-1.13.10/dist/css/bootstrap-select.css',
		CORE_PATH().'plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css',

		CORE_PATH().'plugins/toastr-2.1.3/toastr.min.css',

		CORE_PATH().'plugins/perfect-scrollbar-0.6.10/perfect-scrollbar.css',

		CORE_PATH().'plugins/fontawesome-pro-5.6.1-web/css/all.css',
		CORE_PATH().'plugins/slim/slim.min.css',
		CORE_PATH().'plugins/jquery-ui-1.12.1/jquery-ui.min.css',	

    CORE_PATH().'plugins/bootstrap-sweetalert/sweet-alert.css',
    
		CORE_PATH().'css/base.css',
		CORE_PATH().'css/style.css'
	);
	return $css;
}

function APPLICATION_JS($type='application'){
	return array(
		CORE_PATH().'plugins/js/jquery-3.3.1.min.js',
		CORE_PATH().'plugins/jquery-ui-1.12.1/jquery-ui.min.js',
		CORE_PATH().'plugins/bootstrap-4.3.1/js/popper.min.js',
		CORE_PATH().'plugins/bootstrap-4.3.1/js/bootstrap.min.js',

		CORE_PATH().'plugins/bootstrap-select-1.13.10/dist/js/bootstrap-select.min.js',
		CORE_PATH().'js/config/selectpicker.js',

		CORE_PATH().'plugins/js/moment.min.js',
		CORE_PATH().'plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js',
		CORE_PATH().'js/config/datetimepicker.js',

		CORE_PATH().'plugins/slim/slim.kickstart.min.js',
		CORE_PATH().'core/slimcropper.js',

		CORE_PATH().'plugins/toastr-2.1.3/toastr.min.js',
		CORE_PATH().'js/config/toastr.js',

		CORE_PATH().'plugins/jquery.scrollbar-0.2.11/jquery.scrollbar.min.js',
		CORE_PATH().'js/config/scrollbar.js',
		

		CORE_PATH().'js/core/hideshow.js',
		
		CORE_PATH().'js/core/sidebar.js',

		CORE_PATH().'js/core/truncate_text.js',

		CORE_PATH().'js/core/autocomplete.js',

		CORE_PATH().'js/core/filter.js',
		CORE_PATH().'js/core/ajax_library.js',
		CORE_PATH().'js/core/tablefilter.js',
	

		THEME_PATH().'js/occupations/occupations.js',
		THEME_PATH().'js/transactions/transaction_details.js',
		THEME_PATH().'js/vendors/country_state_city.js',
		THEME_PATH().'js/communications/inapp_users.js',
		THEME_PATH().'js/country_state_cities/country_state_city.js',
		THEME_PATH().'js/base.js',
	);
}

?>