<?php 

function APPLICATION_CSS($type='application'){
	$css =  array(
		CORE_PATH().'plugins/bootstrap-4.3.1/css/bootstrap.min.css',
		CORE_PATH().'plugins/bootstrap-select-1.13.10/dist/css/bootstrap-select.css',
		CORE_PATH().'plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css',

		CORE_PATH().'plugins/toastr-2.1.3/toastr.min.css',

		CORE_PATH().'plugins/perfect-scrollbar-0.6.10/perfect-scrollbar.css',

		CORE_PATH().'plugins/country_code/css/intlTelInput.min.css',
		CORE_PATH().'plugins/fancybox/jquery.fancybox.min.css',
		CORE_PATH().'plugins/circlebars-master/assets/circle.css',
		CORE_PATH().'plugins/slim/slim.min.css',
		CORE_PATH().'plugins/flags/css/flag-icon.min.css',
		CORE_PATH().'plugins/colorpicker/color-picker.min.css',
		CORE_PATH().'plugins/fontawesome-pro-5.6.1-web/css/all.css',
		CORE_PATH().'plugins/jquery-ui-1.12.1/jquery-ui.min.css',	
		CORE_PATH().'plugins/tags-input/tags-input.css',
		CORE_PATH().'plugins/slick-1.8.1/slick.css',
    CORE_PATH().'plugins/bootstrap-sweetalert/sweet-alert.css',
    
		CORE_PATH().'css/base.css',
		CORE_PATH().'css/style.css'
	);
	return $css;
}

function APPLICATION_JS($type='application'){
	return array(
		CORE_PATH().'plugins/js/jquery-3.3.1.min.js',
		CORE_PATH().'plugins/jquery-ui-1.12.1/jquery-ui.min.js',
		CORE_PATH().'plugins/bootstrap-4.3.1/js/popper.min.js',
		CORE_PATH().'plugins/bootstrap-4.3.1/js/bootstrap.min.js',
		CORE_PATH().'config/modal.js',

		CORE_PATH().'plugins/bootstrap-select-1.13.10/dist/js/bootstrap-select.min.js',
		CORE_PATH().'js/config/selectpicker.js',

		CORE_PATH().'plugins/js/moment.min.js',
		CORE_PATH().'plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js',
		CORE_PATH().'js/config/datetimepicker.js',

		CORE_PATH().'plugins/toastr-2.1.3/toastr.min.js',
		CORE_PATH().'js/config/toastr.js',

		CORE_PATH().'plugins/ckeditor/ckeditor.js',
		CORE_PATH().'config/ckeditor.js',

		CORE_PATH().'plugins/sticky-kit-1.1.3/sticky-kit.min.js',

		CORE_PATH().'plugins/jquery.scrollbar-0.2.11/jquery.scrollbar.min.js',
		CORE_PATH().'js/config/scrollbar.js',
		
		CORE_PATH().'plugins/slim/slim.kickstart.min.js',
		CORE_PATH().'core/slimcropper.js',

		CORE_PATH().'plugins/fancybox/jquery.fancybox.min.js',
		CORE_PATH().'config/fancybox.js',

		CORE_PATH().'plugins/country_code/js/intlTelInput-jquery.min.js',
		CORE_PATH().'plugins/country_code/js/setting.js',

		CORE_PATH().'plugins/circlebars-master/assets/circle.js',

		CORE_PATH().'plugins/colorpicker/color-picker.min.js',
		CORE_PATH().'plugins/js/jscolor.js',		
		CORE_PATH().'config/colorpicker.js',

		CORE_PATH().'plugins/chart.js-2.8.0/dist/Chart.min.js',
		CORE_PATH().'config/chart.js',


		CORE_PATH().'config/stick_in_parent.js',
		CORE_PATH().'config/tags_input.js',

		CORE_PATH().'js/core/hideshow.js',
		CORE_PATH().'js/core/sidemenu.js',
		CORE_PATH().'core/autofocus.js',
		CORE_PATH().'core/helptext.js',
		CORE_PATH().'js/core/truncate_text.js',
		CORE_PATH().'core/togglelist.js',
		CORE_PATH().'core/ajaxloader.js',
		CORE_PATH().'js/core/autocomplete.js',
		CORE_PATH().'core/tooltips.js',
		CORE_PATH().'core/print.js',
		CORE_PATH().'core/scroll.js',
		CORE_PATH().'js/core/filter.js',
		CORE_PATH().'js/core/tablefilter.js',



		THEME_PATH().'js/homepage/autocomplete.js',
		THEME_PATH().'js/base.js',




	);
}

?>