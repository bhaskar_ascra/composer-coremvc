<?php 

function LAYOUT_PATH($layout='application'){
	return ADMIN_PATH.'assets/';
}

function JS_PATH($layout = 'application') {
	return ADMIN_PATH.'assets/js/';
}

function CSS_PATH($layout = 'application') {
	return ADMIN_PATH.'assets/css/';
}

function FONT_PATH($layout = 'application') {
	return ADMIN_PATH.'assets/fonts/';
}

function IMAGE_PATH($layout = 'application') {
	return ADMIN_PATH.'assets/images/';
}

function PAGES_PATH($layout = 'application') {
	return ADMIN_PATH.'assets/css/pages/';
}

function CORE_PATH($layout = 'application') {
	return ADMIN_PATH.'assets/core/';
}

function LOGIN_PATH($layout = 'application') {
  return ADMIN_PATH.'assets/login/';
}

function THEME_PATH($layout = 'application') {
	return ADMIN_PATH.'assets/theme/';
}



?>