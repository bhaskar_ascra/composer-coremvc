<?php
function get_enable_value() {
  return array(array('chk_id' => 'yes',
                     'value' => '1',
                     'label' => 'Yes',
                     'id' =>''),
               array('chk_id' => 'no',
                     'value' => '0',
                     'label' => 'No',
                     'id' => ''));
}

function get_status() {
	return array(array('chk_id' => 'verified',
										 'value' => 'verified',
										 'label' => 'Verified',
										 'id' => ''),
							 array('chk_id' => 'not_verified',
										 'value' => 'not_verified',
										 'label' => 'Not Verified',
										 'id' => ''));
}