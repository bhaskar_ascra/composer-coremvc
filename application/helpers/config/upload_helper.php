<?php defined('BASEPATH') OR exit('No direct script access allowed.');

function image_sizes($field_name,$controller){
  $img_sizes = array();
  $ci = &get_instance();
  $file_content = get_file_content($field_name,$controller);
  $folder =  isset($file_content['folder'])?$file_content['folder']:'';
  switch($folder){
    case 'admin/basic_informations' :
      $img_sizes['thumbnail'] = array('width'=>100, 'height'=>100, 'folder'=>'/thumb');
      $img_sizes['small'] = array('width'=>200, 'height'=>200, 'folder'=>'/small');
      $img_sizes['large'] = array('width'=>600,'height'=>600,'folder'=>'/large');
      $img_sizes['medium'] = array('width'=>400,'height'=>400,'folder'=>'/medium');
    break;
    case 'admin/images' :
      $img_sizes['thumbnail'] = array('width'=>100, 'height'=>100, 'folder'=>'/thumb');
      $img_sizes['small'] = array('width'=>200, 'height'=>200, 'folder'=>'/small');
      $img_sizes['large'] = array('width'=>600,'height'=>600,'folder'=>'/large');
      $img_sizes['medium'] = array('width'=>400,'height'=>400,'folder'=>'/medium');
    break;
  }
  return $img_sizes;
}

function get_folder_path($file_identifier){
  $ci = &get_instance();
  switch($file_identifier){
    case 'my_documents_and_images/image_file_name' ://Customers Image Videos
      return 'admin/images';
    break;
    case 'cropped_images/photo_file_name' :
      return 'admin/basic_informations';
  }
  return $file_content;
}

function get_file_url($file_identifier, $record, $style = 'original') {
  if (!empty($record[$file_identifier])) 
    return base_url().get_folder_path($file_identifier)."/".$style."/".$record[$file_identifier];
  else
    return '';
}


function get_file_content($field_name,$controller=''){
  if(!empty($controller))
    $field_name = $controller.'/'.$field_name;
  $ci = &get_instance();
  $file_content = array('upload_on'=> UPLOAD_ON);
    switch($field_name){
      case 'cropped_images/photo_file_name' :
        $folder_array = array('folder'=>'admin/basic_informations');
        $file_content = array_merge($file_content,$folder_array);        
      break;
      case 'my_documents_and_images/doc_file_name' :
        $folder_array = array('folder'=>'resume_informations');
        $file_content = array_merge($file_content,$folder_array);
      break;
      case 'my_documents_and_images/image_file_name' :
        
        $folder_array = array('folder'=>'admin/images');
        $file_content = array_merge($file_content,$folder_array);
      break;
    }    
  return $file_content;
}

function load_image($file_name,$return_url=false){
  $ci = &get_instance();
  return $file_name = $ci->upload_file->get_file_url($file_name,'',$return_url);
}
