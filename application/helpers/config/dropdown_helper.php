<?php
function get_states(){
  return array(
    array('name'=>'Maharashtra',
          'id'=>'Maharashtra'),
    array('name'=>'Gujarat',
          'id'=>'Gujarat'));
}

function get_city() {
  return array(
    array('name'=>'Mumbai',
          'id'=>'Mumbai'),
    array('name'=>'Ahmedabad',
          'id'=>'Ahmedabad'));
}

function get_countries() {
  return array(
    array('name'=>'AUSTRALIA',
          'id'=>'1'),
    array('name'=>'INDIA',
          'id'=>'2'),
    array('name'=>'NEW ZEALAND',
          'id'=>'3'),
    array('name'=>'USA',
          'id'=>'4'),
    array('name'=>'UAE',
          'id'=>'5'),
    array('name'=>'MAURITIUS',
          'id'=>'6'));
}

function get_multiple_states() {
  return array(
    array('name' => '',
          'id' =>'0'),
    array('name'=>'QUEENSLAND',
          'id'=>'1'),
    array('name'=>'ANDHRAPRADESH',
          'id'=>'2'),
    array('name'=>'AUCKLAND',
          'id'=>'3'),
    array('name'=>'NEWJERSEY',
          'id'=>'4'),
    array('name'=>'DUBAI',
          'id'=>'5'),
    array('name'=>'MAURITIUS',
          'id'=>'6'));
}

function get_multiple_cities() {
  return array(
    array('name'=>'',
          'id'=>''),
    array('name'=>'BRISBANE',
          'id'=>'BRISBANE',
          'value'=>'QUEENSLAND'),
    array('name'=>'HYDERABAD',
          'id'=>'HYDERABAD',
          'value'=>'ANDHRAPRADESH'),
    array('name'=>'AUCKLAND',
          'id'=>'AUCKLAND',
          'value'=>'AUCKLAND'),
    array('name'=>'EDISON',
          'id'=>'EDISON',
          'value'=>'NEWJERSEY'),
    array('name'=>'DUBAI',
          'id'=>'DUBAI',
          'value'=>'DUBAI'),
    array('name'=>'MAURITIUS',
          'id'=>'MAURITIUS',
          'value'=>'MAURITIUS'));
}