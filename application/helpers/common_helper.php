<?php

defined('BASEPATH') OR exit('No direct script access allowed.');
/*
if (!function_exists('userdataFromUsername')) {

    function userdataFromUsername($username) {
        $ci = &get_instance();
        $result=$ci->db->get_where("users",array("email"=>$username))->row_array();
        return $result;
    }

}*/

function pd($data, $die=1) {
  echo '<pre>';
  print_r($data);
  if ($die==1)
    die;
}

if ( ! function_exists('pr')) {
  function pr($arr)
  {
    echo '<pre>'; 
    print_r($arr);
    echo '</pre>';
    die;
  }
}

/**
 * [To print last query]
*/
if ( ! function_exists('lq')) {
  function lq()
  {
    $CI = & get_instance();
    echo $CI->db->last_query();
    die;
  }
}

if (!function_exists('get_errorMsg')) {
  function get_errorMsg($msg = "") {
    if ($msg == "")
        $msg = "Oops! Error.  Please try again later!!!";
    $error_msg = array(
        "status" => "error",
        "data" => $msg
    );
    return $error_msg;
  }

}

if (!function_exists('get_validation_errors')) {
  function get_validation_errors($errors,$type=''){
    $validation_errors=array(
        'status'=>'error',
        'errors'=>$errors,
        'error_type'=>$type
    );
    return $validation_errors;
  }
}
if (!function_exists('sanitize_input_text')) {
  function sanitize_input_text($str){
    $CI = & get_instance();  // get instance, access the CI superobject
    return $CI->security->xss_clean($str);  //security library must be autoloaded
  }
}

if (!function_exists('sanitize_output_text')) {
  function sanitize_output_text($str){
    return htmlspecialchars($str);
  }
}

if (!function_exists('get_csrf_token')) {
  function get_csrf_token(){
    $CI = & get_instance();  // get instance, access the CI superobject
    $csrf = array(
      'name' => $CI->security->get_csrf_token_name(),  //csrf token key
      'hash' => $CI->security->get_csrf_hash()  //csrf token value
    );
    return $csrf;
  }
}

if ( ! function_exists('makedirs')) {
  function makedirs($folder='', $mode=DIR_WRITE_MODE){    
    if(!empty($folder)) {
      if(!is_dir(FCPATH . $folder)){
        mkdir(FCPATH . $folder, $mode);
      }
    } 
  }
}

if (!function_exists('custom_url_manager')){
  function custom_url_manager($type='',$page_number=''){
    $url = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];

      if(isset($url) && !empty($url)){
        $ordered = (isset($_GET['ordered_columns']) ? $_GET['ordered_columns'] : '');
        $selected = (isset($_GET['selected_column']) ? $_GET['selected_column'] : '');
        $page_no = (isset($_GET['page_no']) ? $_GET['page_no'] : '');
        $url = str_replace("?select_col=1&table_filter=1","",$url);$url = str_replace("&selected_column=".@$_GET['selected_column']."&ordered_columns=".@$_GET['ordered_columns'],"",$url);

        $url = str_replace("?selected_column=".@$_GET['selected_column']."&ordered_columns=".@$_GET['ordered_columns'],"",$url);
        $url = str_replace("&select_col=","",$url);
        $url = str_replace("&is_ajax=0","",$url);
        $url = str_replace("&is_ajax=1","",$url);
        $url = str_replace("&selected_column=".@$_GET['selected_column']."&ordered_columns=".@$_GET['ordered_columns'],"",$url);
        $url = str_replace("&page_no=".@$_GET['page_no'],"",$url);
        $url = str_replace("?page_no=".@$_GET['page_no'],"",$url);
        $url = str_replace("?page_no=''","",$url);
        
        // SELECT & AGRRANGE COLUMNS
        if(($selected == 1 && $ordered == 1) || $type == 'ordered_columns')
          $url .= get_connector($url)."selected_column=1&ordered_columns=1";
        elseif(($selected == 1 && $ordered != 1) || $type == 'selected_column')
          $url .= get_connector($url)."selected_column=1&ordered_columns=0";
        // PAGINATION
        if($page_no != '' || $type == 'pagination')
          $url .= get_connector($url)."page_no=".$page_number;
        return $url;
      }
      return '';
  }
}

function get_connector($url){
  $connector = '?';
  $is_question = substr_count($url, "?");
  if($is_question>=1)
    $connector = '&';

  return $connector;
}


