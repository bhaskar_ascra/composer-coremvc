<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_create_table_country_cities extends CI_Model {

  public function up()
  {
  	$sql = "CREATE TABLE IF NOT EXISTS `country_state_cities` (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `country` varchar(255) NULL,
					  `state` varchar(255) NULL,
					  `city` varchar(255) NULL,
					  `created_at` datetime NULL,
					  `updated_at` datetime NULL,
					  `is_delete` tinyint(4)  NULL DEFAULT 0,
					  `created_by` varchar(255) NULL,
					  `updated_by` varchar(255) NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";
    $this->db->query($sql);
  }


}

?>