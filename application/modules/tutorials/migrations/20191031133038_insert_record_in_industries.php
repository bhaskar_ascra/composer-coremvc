<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_insert_record_in_industries extends CI_Model {

  public function up()
  {
  	$sql = "INSERT INTO `industries` (`id`, `name`, `industry_type`, `state`, `city`, `last_active_at`, `incorporated_at`,
  	       `total_area`, `enabled`, `status`, `created_at`, `updated_at`, `is_delete`, `created_by`, `updated_by`) VALUES (NULL, 'JP Morgan', 
  	       'Investment Banking', 'Maharashtra', 'Mumbai', '2019-10-29 08:11:14', '2010-05-07 04:15:12', '8500.25', 'yes', 'Verified', 
  	       '2019-10-27 06:10:08', NULL, '0', NULL, NULL);";
    $this->db->query($sql);
  }


}

?>