<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_insert_one_more_record_in_industries extends CI_Model {

  public function up()
  {
  	$sql = "INSERT INTO `industries` (`id`, `name`, `industry_type`, `state`, `city`, `last_active_at`, `incorporated_at`,
  	       `total_area`, `enabled`, `status`, `created_at`, `updated_at`, `is_delete`, `created_by`, `updated_by`) VALUES (NULL, 'Infosys', 
  	       'Business Consulting', 'Gujarat', 'Ahmedabad', '2019-11-22 08:11:14', '2010-08-07 04:15:12', '4500.25', 'yes', 'Verified',
  	       '2019-10-29 06:10:08', NULL, '0', NULL, NULL);";
    $this->db->query($sql);
  }


}

?>