<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_insert_records_in_industries extends CI_Model {

  public function up()
  {
  	$sql = "INSERT INTO `industries` (`id`, `name`, `industry_type`, `state`, `city`, `last_active_at`, `incorporated_at`,
  	       `total_area`, `enabled`, `status`, `created_at`, `updated_at`, `is_delete`, `created_by`, `updated_by`) VALUES (NULL, 'Accenture', 
  	       'Operations', 'Gujarat', 'Ahmedabad', '2019-11-29 08:11:14', '2010-06-07 04:15:12', '3500.25', 'yes', 'Verified', 
  	       '2019-10-28 06:10:08', NULL, '0', NULL, NULL);";
    $this->db->query($sql);
  }


}

?>