<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_create_table_countries extends CI_Model {

  public function up()
  {
  	$sql = "CREATE TABLE IF NOT EXISTS `countries` (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `name_code` varchar(4) NOT NULL,
					  `name` varchar(150) NOT NULL,
					  `phonecode` int(11) NOT NULL,
					  `created_at` datetime NULL,
					  `updated_at` datetime NULL,
					  `is_delete` tinyint(4)  NULL DEFAULT 0,
					  `created_by` int(11) NULL,
					  `updated_by` int(11) NULL,
					  PRIMARY KEY (`id`)
					 )ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";
    $this->db->query($sql);
  }


}

?>