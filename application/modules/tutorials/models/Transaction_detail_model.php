<?php
class Transaction_detail_model extends BaseModel{
  protected $table_name = 'transaction_details';
  protected $id = 'id';
  public function __construct($data=array()) {
    parent::__construct($data);
  }

  public function validation_rules($klass='', $index=0) {
    $rules = array(array( 
      'field' => 'transaction_details['.$index.'][item_code]',
      'label' => 'Name',
      'rules' => array('trim', 
                       'required', 
                       'max_length[5]')));
    return $rules;
  }
}