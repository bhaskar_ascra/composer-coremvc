<?php
class Cropped_image_model extends BaseModel{
  protected $table_name = 'cropped_images';
  protected $id = 'id';
  public function __construct($data=array()) {
    parent::__construct($data);
  }

  public function validation_rules($klass='') {
    $rules = array(
      array('field' => 'cropped_images[image_name]', 'label' => 'Image',
            'rules' => array('trim','required','max_length[255]')));
    if (!(empty($_FILES[$this->router_class]['name']['photo_file_name']) && !empty($this->attributes['id']))) {
      $rules[] = array('field' => 'cropped_images[photo_file_name]', 'label' => 'Image File Name',
                       'rules' => array('trim',
                                        array('required_image', array($this,'required_image')),
                                        array('min_size', array($this,'min_size')),
                                        array('max_size', array($this,'max_size'))),
                       'errors' => array("required_image" => "Please select image.",
                                         "min_size" => "Image is less then 200px*200px,
                                                        Please upload minimumn 200px*200px Image",
                                         "max_size" => 'Image is more then 9000px*9000px,
                                                        Please upload maximum 9000px*9000px Image'));  
    }
    return $rules;
  }

  public function required_image(){
    return (!empty($_FILES[$this->router_class]['name']['photo_file_name']));
  }

  public function min_size(){  
    if(in_array($_FILES[$this->router_class]['type']['photo_file_name'], image_mimetypes())) {
      if(isset($_FILES[$this->router_class]['size']) AND !empty($_FILES[$this->router_class]['size'])){
        if(isset($_FILES[$this->router_class]['tmp_name']['photo_file_name'])){
          $data = getimagesize($_FILES[$this->router_class]['tmp_name']['photo_file_name']);
          return (($data[0] >= 200 OR $data[1] >= 200) AND $_FILES[$this->router_class]['size']['photo_file_name'] < MAX_SIZE);    
        }  
      }  
    }
  }

  public function max_size(){
    if(in_array($_FILES[$this->router_class]['type']['photo_file_name'], image_mimetypes())) {
      if(isset($_FILES[$this->router_class]['size']) AND !empty($_FILES[$this->router_class]['size'])) {
        if(isset($_FILES[$this->router_class]['tmp_name']['photo_file_name'])) {
          $data = getimagesize($_FILES[$this->router_class]['tmp_name']['photo_file_name']);
          return (($data[0] <= 2000 OR $data[1] < 2000) AND $_FILES[$this->router_class]['size']['photo_file_name'] <= MAX_SIZE);
        }  
      }
    }
  }
}