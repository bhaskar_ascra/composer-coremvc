<?php
class Occupation_model extends BaseModel{
  protected $table_name = 'occupations';
  protected $id = 'id';

  public function __construct($data=array()) {
    parent::__construct($data);
  }

  public function validation_rules($klass='') {

    return array(array('field' => 'occupations[occupation_name]',
                       'label' => 'Occupation Name',
                       'rules' => array('trim','required','max_length[255]', 'alpha_numeric_spaces',
                                    array('unique_name', 
                                      array($this, 'name_unique'))) ,
                       'errors'=> array('unique_name' => "Occupation Name must be unique.")),
                 array('field' => 'occupations[state]',
                       'label' => 'State',
                       'rules' => array('trim', 'required')),
                 array('field' => 'occupations[city]',
                       'label' => 'City',
                       'rules' => array('trim', 'required')),
                 array('field' => 'occupations[last_viewed_at]',
                       'label' => 'Last Viewed At',
                       'rules' => array('trim', 'required')),
                 array('field' => 'occupations[last_viewed_at]',
                       'label' => 'Last Viewed At',
                       'rules' => array('trim', 'required')),
                 array('field' => 'occupations[last_viewed_at]',
                       'label' => 'Last Viewed At',
                       'rules' => array('trim',)));
  }

  public function name_unique($occupation_name) {
    return parent::check_unique('occupation_name');
  }
}