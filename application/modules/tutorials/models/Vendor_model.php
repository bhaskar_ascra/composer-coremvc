<?php
class Vendor_model extends BaseModel{
  protected $table_name = 'vendors';
  protected $id = 'id';
  public function __construct($data=array()) {
    parent::__construct($data);
  }

  public function validation_rules($klass=''){
  	$rules = array(array('field' => 'vendors[name]',
                         'label' => 'Name',
                         'rules' => array('trim','required','max_length[255]')),
  					 		   array('field' => 'vendors[country_id]',
                         'label' => 'Country_id',
                         'rules' => array('trim','required')),
  							   array('field' => 'vendors[state_id]',
                         'label' => 'State_id',
                         'rules' => array('trim','required')),
                   array('field' => 'vendors[city_id]',
                         'label' => 'City_id',
                         'rules' => array('trim','required'))
                   );
  	return $rules;
  }
}