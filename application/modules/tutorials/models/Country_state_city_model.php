<?php
class Country_state_city_model extends BaseModel{
  protected $table_name = 'country_state_cities';
  protected $id = 'id';
  public function __construct($data=array()) {
    parent::__construct($data);
  }

  public function validation_rules($klass=''){
  	$rules = array(array('field' => 'country_state_cities[country]',
                         'label' => 'Country',
                         'rules' => array('trim','required','max_length[255]')));
  	return $rules;
  }
}