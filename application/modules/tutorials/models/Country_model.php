<?php

class Country_model extends BaseModel
{
  protected $table_name = 'countries';
  protected $id = 'id';

  public function __construct() {
    parent::__construct();
  }
}
