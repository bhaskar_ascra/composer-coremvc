<?php
class Employee_model extends BaseModel{
  protected $table_name = 'employees';
  protected $id = 'id';

  public function __construct($data=array()) {
    parent::__construct($data);
  }

  public function validation_rules($klass='') {
    return array(array('field' => 'employees[name]',
                       'label' => 'Name',
                       'rules' => array('trim','required','max_length[255]', 'alpha_numeric_spaces')));
  }
}