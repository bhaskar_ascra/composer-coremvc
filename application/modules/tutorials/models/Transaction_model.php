<?php
class Transaction_model extends BaseModel{
  protected $table_name = 'transactions';

  public function __construct($data=array()) {
    parent::__construct($data);
  }

  public function after_save($action) {
    foreach($this->formdata['transaction_details'] as $index => $transaction_detail){
      if(isset($transaction_detail['delete']) AND $transaction_detail['delete']==1 && 
         !empty($transaction_detail['id']))
        $this->transaction_detail_model->delete($transaction_detail['id']);
      unset($transaction_detail['delete']);
      
      $transaction_details = new transaction_detail_model($transaction_detail);
      $transaction_details->attributes['transaction_id'] = $this->formdata['transactions']['id'];
      $transaction_details->attributes['item_code'] = $transaction_detail['item_code'];
      $transaction_details->attributes['quantity'] = $transaction_detail['quantity'];
      $transaction_details->attributes['weight'] = $transaction_detail['weight'];
      $transaction_details->attributes['category_id'] = $transaction_detail['category_id'];
      $transaction_details->save();
    }
  }

  public function validate($validation_klass='') {
    $rules = $this->validation_rules();
    foreach($this->formdata['transaction_details'] as $index => $transaction_detail) {
      $transaction_detail_rules = $this->transaction_detail_model->validation_rules('', $index);
      $rules = array_merge($rules, $transaction_detail_rules);
    }
    $this->form_validation->set_rules($rules);
    $this->form_validation->set_data($this->formdata);
    return $this->form_validation->run();
  }

  public function validation_rules($klass='') {
    $rules = array(array(
      'field' => 'transactions[name]',
      'label' => 'Name',
      'rules' => array('trim', 'required', 'max_length[45]')));
    return $rules;
  }
}