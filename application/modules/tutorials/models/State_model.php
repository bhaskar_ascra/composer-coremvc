<?php

class State_model extends BaseModel
{
  protected $table_name = 'states';
  protected $id = 'id';

  public function __construct() {
    parent::__construct();
  }
}
