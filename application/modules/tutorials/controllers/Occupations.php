<?php
class Occupations extends BaseController {
  public function __construct(){
    parent::__construct();
    $this->redirect_after_save = 'view';
  }

  public function _get_form_data(){
  	if($this->router->method=="create" || $this->router->method=="store")
  		$this->data['form_title']="Create Occupations";
  	if($this->router->method=="edit" || $this->router->method=="update")
  		$this->data['form_title']="Edit Occupations";
  }
}