<?php
class Cropped_images extends BaseController {

  public function __construct(){
    parent::__construct();
    $this->redirect_after_save = 'view';
    $this->data['file_data'] = array(array('file_field_name'=>'photo_file_name',
                                           'file_controller'=>'cropped_images'));
  }
}  