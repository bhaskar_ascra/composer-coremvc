<?php
class Employees extends BaseController {
  public function __construct(){
    parent::__construct();
    $this->redirect_after_save = 'view';
  	$this->load->model(array('occupation_model'));
  }

  public function _get_form_data() {
  	$this->data['occupation_id'] = $this->occupation_model->get('id, occupation_name as name');
  }
}