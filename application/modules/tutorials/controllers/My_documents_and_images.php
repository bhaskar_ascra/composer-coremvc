<?php
class My_documents_and_images extends BaseController {

  public function __construct(){
    parent::__construct();
    $this->redirect_after_save = 'view';
    $this->data['file_data'] = array(array('file_field_name'=>'doc_file_name',
                                           'file_controller'=>'my_documents_and_images'));
    $this->data['file_data'] = array(array('file_field_name'=>'image_file_name',
                                           'file_controller'=>'my_documents_and_images'));
  }
}  