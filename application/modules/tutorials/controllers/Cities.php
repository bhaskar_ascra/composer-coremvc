<?php
class Cities extends CI_Controller {
  public function __construct() {
    parent::__construct();
    $this->load->model(array('City_model')); 
  }

  public function index() {
    if (!empty($_GET['state_id'])) {
      $cities = $this->City_model->get('id, name as name', array('state_id'=>$_GET['state_id']));
      echo json_encode(array('cities' => $cities,
                             'status' => 'success',
                             'open_modal' => FALSE,
                             'ajax_success_function' => 'set_city_options(response)'));die;
    } else
      redirect_to(ADMIN_PATH);
  }
}