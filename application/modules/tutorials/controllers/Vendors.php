<?php
class Vendors extends BaseController {
  public function __construct() {
    parent::__construct();
    $this->load->model(array('Country_model','State_model','City_model'));
  }
  public function _get_form_data(){
  	if ($this->router->method=='create'){
      $this->data['states'] = array();
      $this->data['cities'] = array();
    } elseif ($this->router->method=='edit'){
      $state_ids = array($this->data['record']['state_id']);
      $city_ids = array($this->data['record']['city_id']);
    	$state_ids = implode(', ', $state_ids);
      $city_ids = implode(', ', $city_ids);
      if(!empty($state_ids) && !empty($city_ids)){
        $this->data['states'] = $this->State_model->get('id, name as name', array('id in ('.$state_ids.")"));
        $this->data['cities'] = $this->City_model->get('id, name as name', array('id in ('.$city_ids.")"));
      }else{
        $this->data['states'] = array();
        $this->data['cities'] = array();
      }
    } else{
      $state_ids = array(@$this->data['record']['state_id']);
      $city_ids = array(@$this->data['record']['city_id']);
    	$state_ids = implode(', ', $state_ids);
      $city_ids = implode(', ', $city_ids);
      if(!empty($state_ids) && !empty($city_ids)){
        $this->data['states'] = $this->State_model->get('id, name as name', array('id in ('.$state_ids.")"));
        $this->data['cities'] = $this->City_model->get('id, name as name', array('id in ('.$city_ids.")"));
      }else{
        $this->data['states'] = array();
        $this->data['cities'] = array();
      }
    }
    $this->data['countries'] = $this->Country_model->get('id, name as name');
  }
}