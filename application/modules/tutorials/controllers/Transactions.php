<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transactions extends BaseController {
  public function __construct(){
    parent::__construct();
    $this->redirect_after_save = 'view';
    $this->load->model(array('transaction_detail_model'));
  } 
  
  public function _get_form_data(){
    if ($this->router->method=='create')
      $this->data['transaction_details'] = array();
    elseif ($this->router->method=='edit')
      $this->data['transaction_details'] = $this->transaction_detail_model->get('',
                                             array('transaction_id' =>$this->data['record']['id']));
    else
      $this->data['transaction_details'] = $_POST['transaction_details'];
  }

  public function _get_view_data(){
    $this->data['transaction_details'] = $this->transaction_detail_model->get('', 
                                            array('transaction_id' =>$this->data['record']['id']));
  }
}