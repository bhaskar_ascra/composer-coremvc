<?php
class States extends CI_Controller {
  public function __construct() {
    parent::__construct();
    $this->load->model(array('State_model')); 
  }
  
  public function index() {
    if (!empty($_GET['country_id'])) {
      $states = $this->State_model->get('id, name as name', array('country_id'=>$_GET['country_id']));
      echo json_encode(array('states' => $states,
                             'status' => 'success',
                             'open_modal' => FALSE,
                             'ajax_success_function' => 'set_state_options(response)'));die;
    } else
      redirect_to(ADMIN_PATH);
  }
}