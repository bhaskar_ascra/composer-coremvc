<h5 class="heading">Transactions</h5>
<div class="row">
  <?php load_field('label_with_value', array('field' => 'name')) ?>
  <?php load_field('label_with_value', array('field' => 'date')) ?>
</div>
  <table class="table table-sm table-default">
  <thead>
    <tr>
      <th>Item_Code</th>
      <th>Quantity</th>
      <th>Weight</th>
      <th>Category ID</th>
    </tr>
  </thead>
  <?php 
    foreach($transaction_details as $index => $transaction_detail) {
      $this->load->view('transaction_details/subview.php', array('index' => $index));
    }
  ?>
</div>  
  