<table class="table table-sm table-default">
  <thead>
    <tr>
      <th>Item_Code</th>
      <th>Quantity</th>
      <th>Weight</th>
      <th>Category ID</th>
      <th></th>
    </tr>
  </thead>
  <tbody id="transaction_detail">
    <?= getJsButton('Add', '#', 'btn btn-sm btn_blue float-right mb-2', '', 'add_transaction_detail_subform()'); ?>
    <?php 
      foreach ($transaction_details as $index => $transaction_detail) {
        $this->load->view('transaction_details/subform.php', array('index' => $index));
      } 
    ?>
  </tbody>
</table>

<script>
  <?php $transaction_detail_form_html =  $this->load->view('transaction_details/subform', 
                                                     array('index' => 'index_count'), TRUE); ?>
  var transaction_detail_form_html = <?= json_encode(array('html' => $transaction_detail_form_html)); ?>;
  var fields_index = <?= time() ?>;

  function add_transaction_detail_subform() {
    var html_str = transaction_detail_form_html.html.replace(/\index_count/g, fields_index);
    fields_index += 1;
    $('#transaction_detail').append(html_str);
    return false;
  }
</script>