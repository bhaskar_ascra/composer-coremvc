<tr>
  <td>
    <?php load_field('hidden', array('field' => 'id',
                                     'index' => $index,
                                     'controller' => 'transaction_details')); ?>
                                     
    <?php load_field('only_value', array('field' => 'item_code',
                                         'index' => $index,
                                         'controller' => 'transaction_details',
                                         'col'=>'col-12')); ?>
  </td>
  <td>
    <?php load_field('only_value', array('field' => 'quantity',
                                         'index' => $index,
                                         'controller' => 'transaction_details',
                                         'col'=>'col-12')); ?>
  </td>
  <td>
    <?php load_field('only_value', array('field' => 'weight',
                                         'index' => $index,
                                         'controller' => 'transaction_details',
                                         'col'=>'col-12')); ?>
  </td>
  <td>
    <?php load_field('only_value', array('field' => 'category_id',
                                         'index' => $index,
                                         'controller' => 'transaction_details',
                                         'col'=>'col-12')); ?>
  </td>
</tr>
