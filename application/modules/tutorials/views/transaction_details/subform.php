<tr class="transaction_details_<?= $index ?>">
  <td>
    <?php load_field('hidden', array('field' => 'id',
                                     'index' => $index,
                                     'controller' => 'transaction_details')); ?>

    <?php load_field('plain/text', array('field' => 'item_code',
                                   'index' => $index,
                                   'controller' => 'transaction_details')); ?>
  </td>
  <td>
    <?php load_field('plain/text', array('field' => 'quantity',
                                   'controller' => 'transaction_details',
                                   'index' => $index)); ?>
  </td>
  <td>
    <?php load_field('plain/text', array('field' => 'weight',
                                   'controller' => 'transaction_details',
                                   'index' => $index)); ?>
  </td>
  <td>
    <?php load_field('plain/text', array('field' => 'category_id',
                                   'controller' => 'transaction_details',
                                   'index' => $index)); ?>
  </td>
  <td>
    <?= getJsButton('Delete', '#', 'red', '', 'delete_transaction_detail('.$index.')'); ?>
    <?php load_field('hidden', array('field' => 'delete',
                                     'controller' => 'transaction_details',
                                     'index' => $index)); ?>
  </td>
</tr>