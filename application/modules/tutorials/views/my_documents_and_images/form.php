<form method="post" class="form-horizontal fields-group-sm form_radius_none" enctype="multipart/form-data"
      action="<?= get_form_action($controller, $action, $record) ?>">
  <div class="row">
    <?php if ($action == 'edit' || $action == 'update'): ?>
     <?php load_field('hidden', array('field' => 'id')) ?>
    <?php endif; ?>
    <?php load_field('text', array('field' => 'doc_name')) ?> 
    <?php load_field('file', array('field' => 'doc_file_name')) ?>
    <?php load_field('text', array('field' => 'image_name')) ?> 
    <?php load_field('file', array('field' => 'image_file_name')) ?>
  </div>
  <?php load_buttons('submit', array('controller' => $controller,
                                     'name' => 'Save',
                                     'class' => 'btn-sm btn_blue')) ?>
</form>

