<form method="post" class="form-horizontal fields-group-sm form_radius_none" enctype="multipart/form-data"
      action="<?= get_form_action($controller, $action, $record) ?>">
  <div class="row location_row">
    <?php if ($action == 'edit' || $action == 'update'): ?>
      <?php load_field('hidden', array('field' => 'id')) ?>
    <?php endif; ?>
    <?php load_field('text', array('field' => 'name')) ?>
    <?php load_field('dropdown',array('field' => 'country_id',
                                      'option' => $countries)) ?>
    <?php load_field('dropdown',array('field' => 'state_id',
                                      'option' => $states)) ?>
    <?php load_field('dropdown',array('field' => 'city_id',
                                      'option' => $cities)) ?>
  </div> 
  <?php load_buttons('submit', array('name' => 'SAVE',
                                     'class'=> 'btn_blue')); ?>
</form>

<script type="text/javascript">
  var countries = <?= json_encode(get_records_by_id($countries)) ?>;
  var states = <?= json_encode(get_records_by_id($states)) ?>; 
</script>

                         
