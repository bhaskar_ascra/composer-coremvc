<form method="post" class="form-horizontal form-group-md form_radius_none" enctype="multipart/form-data"
      action="<?= get_form_action($controller, $action, $record) ?>">
  <div class="row">
    <?php if ($action == 'edit' || $action == 'update'): ?>
     <?php load_field('hidden', array('field' => 'id')) ?>
    <?php endif; ?>
    <?php 
        if(isset($record['photo_file_name']) && $record['photo_file_name']){
          $image_url = 'admin/basic_informations/small/'.$record['photo_file_name'];
        }else{
          $image_url = '';
      }?>
    <?php load_field('text', array('field' => 'image_name')) ?> 
    <?php load_field('plain/photo', array('field' => 'photo_file_name',
                                          'value' => $image_url,
                                          'image'=>$image_url,
                                          'id'=>'modalCropper',
                                          'col'=>'col-12')) ?>
  </div>
  <?php load_buttons('submit', array('controller' => $controller,
                                     'name' => 'SAVE',
                                     'class' => 'btn_blue')) ?>
</form>

