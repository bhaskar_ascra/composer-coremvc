<form method="post" class="form-horizontal form-group-md form_radius_none" enctype="multipart/form-data"
      action="<?= get_form_action($controller, $action, $record) ?>">
  <div class="row">
    <?php if ($action == 'edit' || $action == 'update'): ?>
     <?php load_field('hidden', array('field' => 'id')) ?>
    <?php endif; ?>
    <?php load_field('text', array('field' => 'name')) ?>
    <?php load_field('dropdown', array('field'=>'occupation_id',
                                       'option'=>$occupation_id)) ?>
  </div>
  <?php load_buttons('submit', array('name' => 'SAVE',
                                     'class' =>'btn_blue')); ?>
</form>