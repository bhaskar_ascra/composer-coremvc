<form method="post" class="form-horizontal fields-group-sm form_radius_none" enctype="multipart/form-data"
      action="<?= get_form_action($controller, $action, $record) ?>">
  <div class="row">
    <?php if ($action == 'edit' || $action == 'update'): ?>
     <?php load_field('hidden', array('field' => 'id')) ?>
    <?php endif; ?>
    <?php load_field('text', array('field' => 'occupation_name')) ?>
    <?php load_field('dropdown', array('field' => 'state',
                                             'option'=>get_states())) ?>
    <?php load_field('dropdown', array('field' => 'city',
                                             'option'=>get_city())) ?>
    <?php load_field('time', array('field' => 'last_viewed_at',
    															 'class' => 'timepicker_js')) ?>
		<?php load_field('date', array('field' => 'created_at',
    															 'class' => 'datepicker_js')) ?>
		<?php load_field('textarea', array('field' => 'comments')) ?>			 
    <?php load_field('checkbox', array('field' => 'enabled',
    																'option'=> get_enable_value())) ?>
		<?php load_field('radio', array('field' => 'status',
																		'option' => get_status()))?>    																
  </div>
  <?php load_buttons('submit', array('name' => 'Save',
                                     'class' => 'btn-sm btn_blue')); ?>
</form>