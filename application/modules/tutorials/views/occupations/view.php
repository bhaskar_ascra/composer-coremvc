<h5 class="heading">Basic Details</h5>
<a href=<?= ADMIN_PATH.'tutorials/occupations/edit/'.$record['id'] ?>>Edit</a>
<div class="row">    
  <?php load_field('label_with_value', array('field' => 'occupation_name')) ?>
  <?php load_field('label_with_value', array('field' => 'state')) ?>
  <?php load_field('label_with_value', array('field' => 'city')) ?>
  <?php load_field('label_with_value', array('field' => 'created_at')) ?>
  <?php load_field('label_with_value', array('field' => 'comments')) ?>
  <?php load_field('label_with_value', array('field' => 'enabled')) ?>
  <?php load_field('label_with_value', array('field' => 'status')) ?>
</div>