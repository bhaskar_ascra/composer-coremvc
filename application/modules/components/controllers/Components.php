<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Components extends BaseController {
	
	public function view($id){
  	$this->data = $this->professional_model->get('',array('users.id='.$id));
  	$this->load->render('view', $this->data);
  }

	 public function __construct() {
   
    $this->load->helper('modules/components_helper');
  }  

	public function index()
	{
		$data['record'] = '';
		$this->load->render('index', $data);
	}
	public function formview()
	{
		$data['record'] = '';
		$this->load->render('formview', $data);
	}
	public function form()
	{
		$data['record'] = '';
		$this->load->render('form', $data);
	}

	public function table()
	{
		$data['record'] = '';
		$this->load->render('table',$data);
	}

	public function open_model(){
		$postData = $this->input->post('modal_name');
		$controller = $this->input->post('controller');
		$view = $controller.'/'.$postData;
		$data['record'] = '';
		$html =  $this->load->view($view ,$data,true);
		$res = json_encode(array('status'=>1,'html'=>$html));
		echo $res;
	}
}
