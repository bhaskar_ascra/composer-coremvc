<?php load_buttons('button', array(
  'name'=>'Button with Modal',
  'class'=>'btn-sm btn_green',
  'type'=>'button',
  'data-toggle'=>'modal',
	'data-target'=>'#modal_form_js'
)); ?>

<?php 
load_buttons('button', array(
  'name'=>'Modal Ajax',
  'class'=>'btn-sm btn_green',
  'type'=>'button',  
	'onclick'=>'modal_form_ajax_js("components","open_model","table","4451")'
)); ?>

<?php load_buttons('button', array(
  'name'=>'Modal Ajax',
  'class'=>'btn-sm btn_green',
  'type'=>'button',  
	'onclick'=>'modal_form_ajax_js("components","open_model","form","4451")'
)); ?>
