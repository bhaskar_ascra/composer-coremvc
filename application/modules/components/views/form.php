<?php
  if (!isset($record)) 
    $record = array();
?>

<form method="post" class="form-horizontal fields-group-sm form_radius_none" 
      enctype="multipart/form-data">
  <div class="row">
    <div class="col-md-8">
    		<!-- Inline Input text with autofocus --> 
        <?php load_field('col/text', array('field' => 'name', 'col'=>'', 'class'=>'helptext_focus_js', 'id'=>'name', 'autofocus'=>true)) ?>
        		              
        <?php load_field('text', array('field' => 'email', 'col'=>'', 'class'=>'helptext_focus_js email_js', 'id'=>'email')) ?>

        <?php load_field('plain/text', array('field' => 'placeholder', 'col'=>'', 'readonly'=>true)) ?>	

        <!-- Default column col-md-6 --> 
        <div class="row mt-3">
        	<?php load_field('text', array('field' => 'column', 'disabled'=>true)) ?>
        <!-- Dropdown With Live Search --> 
	        	<?php load_field('dropdown', 
	        		array('field' => 'dropdown', 'livesearch'=>true, 
			        			'option'=>array(
			        			array('id' => 1, 'name'=>'First Option'),
			        			array('id' => 2, 'name'=>'Second Option'),
			        			array('id' => 3, 'name'=>'Third Option')
	        	))) ?>

        		<?php load_field('dropdown', 
        		array('field' => 'dropdown_multiple', 'multiple'=>true, 
		        			'option'=>array(
		        			array('id' => 1, 'name'=>'First Option'),
		        			array('id' => 2, 'name'=>'Second Option'),
		        			array('id' => 3, 'name'=>'Third Option')
        		))) ?>	

        		<?php load_field('dropdown', 
        		array('field' => 'dropdown_multiple',
		        			'option'=>array(
		        			array('id' => 1, 'name'=>'First Option'),
		        			array('id' => 2, 'name'=>'Second Option'),
		        			array('id' => 3, 'name'=>'Third Option')
        		))) ?>

            <?php load_field('checkbox', array(
                             'field' => 'name',
                             'toggle'=>'test',
                             'target'=>'target',                              
                             'option' => array(
                                         array('label' => 'Replacement',
                                                'value' => '1',                                                
                                              )
            ))); ?>


            <?php load_field('radio', array('field' => 'name',
                              'option'=> array(
                                  array(
                                    'value' => '1', 
                                    'checked'=>'checked',
                                    'label' => 'Agree'),                                  
              ))); ?>

            <?php
              load_field('radio_btn', array('field' => 'column',
                              'option'=> array(
                                  array(
                                    'value' => '1', 
                                    'checked'=>'checked',
                                    'label' => 'Yes',
                                    'chk_id' => 'yes1'
                                  ),
                                  array(
                                    'value' => '0',                
                                    'label' => 'No',
                                    'chk_id' => 'No2'
                                  ),
              )));



              load_field('col/checkbox_btn', array('field' => 'email',
                              'option'=> array(
                                  array(
                                    'value' => '1', 
                                    'checked'=>'checked',
                                    'label' => 'Yes',
                                    'chk_id' => '3Yes'
                                  ),
                                  array(
                                    'value' => '0',                                       
                                    'label' => 'No',
                                    'chk_id' => '4No'
                                  ),
              )));
            ?>

        		<?php load_field('col/textarea', array('field' => 'textarea', 'col'=>'col-md-12')) ?>  
        </div>

        		<?php load_buttons('submit', array(
						  'name'=>'Submit Button LG',
						  'class'=>'btn-lg btn_blue'
						)); ?>
						<?php load_buttons('button', array(
						  'name'=>'Button MD',
						  'class'=>'btn-md btn_red'
						)); ?>
						<?php load_buttons('button', array(
						  'name'=>'Button',
						  'class'=>'btn-sm btn_green',
						  'type'=>'button',						  
						)); ?>
						<?php load_buttons('anchor', array(
						  'name'=>'Anchor Button sm',
						  'class'=>'btn-sm blue link'
						)); ?>


     <?php load_field('date', array('field' => 'email', 'class'=>"datepicker_js")) ?>

     <?php load_field('col/file', array('field' => 'email')) ?>
    </div>
    <div class="col-md-4 border-left">      
      <div class="helptext helptext_js" id="tags_name">
          <h5>Name <small class="medium blinking">(Click to Select)</small></h5>
         <ul class="list-unstyled choosetext_js">    
          <li>Name_1</li>        
          <li>Name_2</li>        
          <li>Name 3</li>
        </ul>      
      </div>
      <div class="helptext helptext_js" id="tags_email">                
          <h5>Email <small class="medium blinking">(Click to Select)</small></h5>
         <ul class="list-unstyled choosetext_js">    
          <li>Email 1</li>        
          <li>Email 2</li>        
          <li>Email 3</li>
        </ul>      
      </div>       
    </div>

    <hr>

     
  </div>
</form>

