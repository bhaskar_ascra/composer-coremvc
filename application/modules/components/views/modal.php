<!-- Modal -->
<div class="modal fade custom_modal" id="modal_form_js" role="dialog">
  <div class="modal-dialog modal-xl">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header bg_blue white">
        <h5 class="modal-title">Modal Header</h5>
        <button class="btn btn-lg btn_blue btn_icon" data-dismiss="modal"><i class="fal fa-times font20"></i></button>
      </div>
      <div class="modal-body">
        <?php $this->load->view('form') ?>
      </div>      
    </div>

  </div>
</div>