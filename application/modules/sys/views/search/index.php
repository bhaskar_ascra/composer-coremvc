<?php $yesNo = array() ?></tr>
<div class="modal fade new_modal" id="searchModal" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content request_comments">
      <div class="modal-header bg_blue white">
        <h3 class="modal-title text-left">Filter</h3>
        <button class="btn btn-lg btn_icon white" data-dismiss="modal"><i class="fal fa-times font20"></i></button>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" id="searchFrom">
          <?php //pr($heading);
          foreach ($heading as $k => $val) {
          if( isset($param['key']) && $val[1] == $search_param) {  ?>
              <div class="form-group">
                <label class="control-label m-b-10"><?=$val[0]?></label>
                <?php if (isset($select_data) && isset($val[9]) && $val[9] == 'dynamic_dropdown') { ?>
                  <div class="col-sm-12">
                    <select name="<?='where['.$val[1].'=]' ?>" class="form-control">
                      <option value="" selected="selected" disabled="disabled">Select Status</option>
                    <?php foreach ($select_data as $option) { ?>
                      <option value="<?= $option; ?>" ><?= strtoupper($option); ?></option>
                    <?php } ?>
                    </select>
                  </div>
                <?php } 
                elseif(isset($val[9]) && $val[9] == 'static_dropdown'){?>
                  <div class="col-sm-12">
                    <select name="<?='where['.$val[1].'=]'; ?>" class="form-control">
                      <option value="" selected="selected" disabled="disabled">Select Status</option>
                    <?php foreach ($val[10] as $option) { ?>
                      <option value="<?= $option; ?>" ><?= $option ?></option>
                    <?php } ?>
                    </select>
                  </div>
                <?php 
                }else if (isset($param['key']) && isset($val[9]) && $val[9] == 'daterange') {?>
                  <div class="row">
                    <div class="col-sm-6">
                      <input type="text"
                             placeholder="MM-DD-YYYY" class="form-control datepicker_js" 
                             name="<?='where['.$val[1].'>=]';?>"
                             value="<?= @$_GET[$val[1]][0]?>">
                    </div>
                    <div class="col-sm-6">
                      <input type="text"
                             placeholder="MM-DD-YYYY" class="form-control datepicker_js" 
                             name="<?='where['.$val[1].'<=]';?>"
                             value="<?= @$_GET[$val[1]][1]?>">
                    </div>                    
                  </div>
                <?php }
                else if (isset($param['key']) && isset($val[9]) && $val[9] == 'range') { ?>
                  <div class="row">
                    <div class="col-sm-6">
                      <input type="text"
                             placeholder="Starting Range" class="form-control " 
                             name="<?='where['.$val[1].'>=]' ?>"
                             value="<?= @$_GET[$val[1]][0]?>" >
                    </div>
                    <div class="col-sm-6">
                      <input type="text"
                             placeholder="End Range" class="form-control " 
                             name="<?='where['.$val[1].'<=]' ?>"
                             value="<?= @$_GET[$val[1]][1]?>" >
                    </div>                    
                  </div>
                <?php }
                 else if (isset($select_data) 
                          && isset($val[9]) 
                          && $val[9] == 'dynamic_multiselect') { ?>
                  <div class="col-sm-12">

                    <select name="like[<?= $val[1].'][]' ?>" class="form-control" multiple>
                      <option value="" selected="selected" disabled="disabled">Select Status</option>
                    <?php $dm= 0;foreach ($select_data as $option) { ?>                      
                      <option value="<?= $option ?>" <?php if(@$_GET[$val[1]] && in_array($option,@$_GET[$val[1]])){echo 'selected="selected"';}?>><?= $option ?></option>
                    <?php  $dm++;} ?>
                    </select>
                  </div>
                <?php } else if (isset($param['key']) 
                                 && isset($val[9]) 
                                 && $val[9] == 'static_multiselect') { ?>
                  <div class="col-sm-12">
                    <select name="like[<?= $val[1].'][]' ?>" class="form-control" multiple>
                      <option value="" selected="selected" disabled="disabled">Select Status</option>
                    <?php $sm = 0;foreach ($val[10] as $option) { ?>                      
                      <option value="<?= $option ?>"
                              <?php 
                                if(@$_GET[$val[1]] && in_array($option,@$_GET[$val[1]])){
                                  echo 'selected="selected"';
                                }?>>
                        <?= $option ?>
                      </option>
                    <?php $sm++;} ?>
                    </select>
                  </div>
                <?php } else if (isset($param['key']) && isset($val[9]) && $val[9] == 'date') { ?>
                  <div class="ui-front">
                    <input type="text" placeholder="MM-DD-YYYY" 
                          class="form-control datepicker_js" 
                          name="where[<?=$val[1] ?>]"
                          value="<?= @$_GET[$val[1]]?>">
                  </div>
                <?php } elseif(isset($val[9]) && $val[9] == 'autocomplete' AND isset($val[10]) AND is_array($val[10])) { ?>
                  <div class="ui-front">
                    <input type="text" placeholder="<?=$val[0] ?>" 
                          class="form-control autocomplete" 
                          name="where[<?=$val[1] ?>]" 
                          data-table="<?= $val[10][0]?>" 
                          data-column= "<?= $val[10][1]?>"
                          value="<?= @$_GET[$val[1]]?>">
                    <div id="suggesstions"></div>
                  </div>
                <?php }else { ?>
                  <div class="ui-front">
                    <input type="text" placeholder="<?=$val[0] ?>" 
                          class="form-control" 
                          name="where[<?=$val[1]; ?>]"
                          value="<?= @$_GET[$val[1]]?>">
                  </div>
                <?php } ?>
              </div>
          <?php  }}
        if(isset($_SESSION['query_string_filter']) AND 
                                                  !empty($_SESSION['query_string_filter'])):
          $get_array = $_SESSION['query_string_filter'];
       // pr($get_array);
          ?>
          <div class="form-group">
            <?php 
            foreach($get_array as $key_end_array => $end_value){
              foreach($heading as $heading_value){
                if(!is_array($end_value)){
                  if($heading_value[1] == $key_end_array AND $param['search_param'] != $heading_value[1]){ 
                    $heading_set = $heading_value[0];?>
                     <div class="form-group divInput">
                       <label class="control-label"><?php echo $heading_set;?></label>
                        <div class="input-group">
                          <input readonly type="text" placeholder="<?= $heading_set; ?>" value="<?=$end_value ?>"
                                 class="form-control" name="where[<?= $key_end_array; ?>]">
                          <div class="input-group-append">    
                            <button class="btn btn-danger removeInput" type="button"><i class="fa fa-trash"></i></button> 
                          </div>
                      </div>
                    </div> </br>
                 <?php }
                }else{
                 if(($heading_value[1] == $key_end_array) AND $param['search_param'] != $heading_value[1] && $heading_value[9] != 'daterange' && $heading_value[9] != 'range'){ 
                  $heading_set = $heading_value[0];?>
                   <div class="form-group divInput">
                   <label class="control-label"><?php echo $heading_set;?></label>
                    <?php foreach($end_value as $multiselect_values){?>
                      <div class="input-group">
                        <input readonly type="text" placeholder="<?= $heading_set; ?>" value="<?=$multiselect_values ?>"
                               class="form-control" name="like[<?= $key_end_array.'][]'; ?>">
                          <div class="input-group-append">    
                            <button class="btn btn-danger removeInput" type="button"><i class="fa fa-trash"></i>
                          </button> 
                        </div>
                    </div></br> 
                    <?php }?>
                  </div>   
                <?php 
                  }
                  if($heading_value[1] == $key_end_array AND $param['search_param'] != $heading_value[1] && $heading_value[9] == 'daterange' ){ 
                  $heading_set = $heading_value[0];?>
                  <div class="form-group divInput">
                  <label class="control-label"><?php echo $heading_set;?></label>
                    <div class="row">
                      <?php foreach($end_value as $multiselect_values){?>
                        <div class="col-sm-6">  
                          <div class="input-group">
                            <input readonly type="text" placeholder="<?= $heading_set; ?>" value="<?=$multiselect_values ?>"
                                   class="form-control" name="where[<?= $key_end_array; ?>[]]">
                              <div class="input-group-append">    
                                <button class="btn btn-danger removeInput" type="button"><i class="fa fa-trash"></i>
                              </button> 
                            </div>
                        </div>
                      </div>
                    <?php }?>  
                    </div> 
                  </div>     
                  <?php 
                  }
                  if($heading_value[1] == $key_end_array AND $param['search_param'] != $heading_value[1] && $heading_value[9] == 'range' && $heading_value[9] != 'daterange' ){ 
                  $heading_set = $heading_value[0];?>
                  <div class="form-group divInput">
                  <label class="control-label"><?php echo $heading_set;?></label>
                    <div class="row">
                      <?php foreach($end_value as $multiselect_values){?>
                        <div class="col-sm-6">  
                          <div class="input-group">
                            <input readonly type="text" value="<?=$multiselect_values ?>"
                                   class="form-control" name="where[<?= $key_end_array; ?>[]]">
                              <div class="input-group-append">    
                                <button class="btn btn-danger removeInput" type="button"><i class="fa fa-trash"></i>
                              </button> 
                            </div>
                        </div>
                      </div>
                    <?php }?>  
                    </div> 
                  </div>     
                  <?php 
                  }
                }  
              }?>
            <?php 
            }?>  
          </div>
          <?php endif;?>      
          <input type="hidden" value="<?= @$param['search_url'] ?>" id="filter_url">
        </form>
      </div>
      <div class="modal-footer">
        <?php
         if($this->input->is_ajax_request() == true)$set_header = 'get_html=1';
        else $set_header = '';
        ?>
         <a href="#" type="button" onclick="filterTableData('<?= $current_url.@$param['search_url'] ?>','<?php echo $param['search_url'];?>','<?php echo $set_header;?>','<?=$param['is_ajax']?>');" class="btn btn_blue">Search
          </a>
      </div>
    </div>      
  </div>
</div>