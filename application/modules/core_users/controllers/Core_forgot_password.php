<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Core_forgot_password extends BaseController {
  public function __construct() {
    parent::__construct();
    $this->load->model(array('email/Email_model', 'User_model'));
    $this->data['layout'] = 'login';
  }

  public function store(){
    parent::update(0);
  }

  public function _before_save($formdata, $action){
    $user = $this->User_model->get('id',array("email_id" => $formdata['forgot_password']['email_id']));
    if(!empty($user)):
      $formdata['forgot_password']['id'] = $user[0]['id'];
      $formdata['forgot_password']['reset_token'] = encrypt();
    endif;
    return $formdata;
  }

  public function _after_save($formdata, $action){
    $user_data = $this->User_model->get('',array("email_id" => $formdata['email_id']));

    // $this->Email_model->send('Forgot Password', 'Email_view', $email_data, $email_data['email']);
    redirect(ADMIN_PATH."users/reset_password/edit/".$user_data[0]['reset_token']);
  }
  
}
?>
