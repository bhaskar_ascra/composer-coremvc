<?php
class Core_user_model extends BaseModel {
  protected $table_name = 'users';
  protected $id = 'id';

  public function __construct($data=array()) {
    parent::__construct($data);
    $this->load->model(array('users/Users_user_role_model'));
  }

  public function after_save($action) {
    $this->save_user_roles();
  }

  private function save_user_roles(){
    if(!empty($this->formdata['users_user_roles']['user_role_id'])):
      $this->Users_user_role_model->delete('',array('user_id' => $this->attributes['id']),true);
      foreach($this->formdata['users_user_roles']['user_role_id'] as $index => $user_role_id):
        $Users_user_role  = new Users_user_role_model();
        $Users_user_role->attributes['user_role_id'] = $user_role_id;
        $Users_user_role->attributes['user_id'] = $this->attributes['id'];
        $Users_user_role->save();
      endforeach;
    endif;
  }

  public function validate($validation_klass='') {
    $this->before_validate();
    $rules = $this->validation_rules();
    $users_user_roles_rules = $this->Users_user_role_model->validation_rules('');
    $rules = array_merge($rules, $users_user_roles_rules);
    $this->form_validation->set_rules($rules);
    $this->form_validation->set_data($this->formdata);
    return $this->form_validation->run();
  }

  public function validation_rules($klass='') {
    $basic_validation = array();
    $password_validation = array();
    $rules = array();

    $basic_validation =  array(
                          array('field' => 'users[name]', 'label' => 'User Name', 
                                'rules' => 'trim|required|max_length[50]'),
                          array('field' => 'users[email_id]', 'label' => 'Email',
                                'rules' => array('trim', 'required', 'max_length[50]', 'valid_email',
                                           array('unique_email', array($this, 'is_email_unique'))),
                                'errors'=> array('unique_email' => "Email already exists.")),
                          array('field' => 'users[mobile_no]', 'label' => 'Contact No',
                                'rules' => 'trim|required|max_length[50]'));
    if($this->router->method == 'create' || $this->router->method == 'store'){
      $password_validation = array(
                              array('field' => 'users[password]', 'label' => 'Password',
                                    'rules' => 'trim|required|max_length[255]|matches[confirm_password]'),
                              array('field' => 'confirm_password', 'label' => 'Confirm Password',
                                    'rules' => 'trim|required|max_length[50]'));
    }
    return $rules = array_merge($basic_validation, $password_validation);
  }

  public function is_email_unique($email_id) {
    $where_conditions = array('email_id' => $email_id);
    if(isset($this->attributes['id']))
      $this->db->where('id!=', $this->attributes['id']);
    $result = $this->get('id', $where_conditions);
    return (empty($result)) ? true : false;
  }

  public function before_save($action){
    if($action == 'store'){
      $this->attributes['encrypted_password'] = md5($this->attributes['password']);
      unset($this->attributes['password']);
    }
  }

  public function set_user_data_in_session($where_condition){
    $user = $this->User_model->find('',$where_condition);
    $user_role_ids = $this->get_user_role_ids($user['id']);
    
    return  array(
              'user_id'         => $user['id'],
              'name'            => $user['name'],
              'email_id'        => $user['email_id'],
              'last_sign_in_at' => $user['last_sign_in_at'],
              'user_role_ids'   => $user_role_ids
            );
  }

  private function get_user_role_ids($user_id) {
    $user_roles = $this->Users_user_role_model->get('user_role_id', array('user_id='.$user_id));
    // $user_role_ids = implode(', ', array_column($user_roles, 'user_role_id'));
    return array_column($user_roles, 'user_role_id');
  }
}