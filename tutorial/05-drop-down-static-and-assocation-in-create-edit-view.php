Association dropdown using Ajax

Resource: Vendors
Module: masters
url: http://localhost/encop_powertrans_new/masters/vendors

1a. Queries: queries/masters/vendors.sql
              CREATE TABLE `vendors` (
                 `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
                 `name` varchar(255),
                 `country_id` int(11) unsigned,
                 `state_id` int(11) unsigned,
                 `city_id` int(11) unsigned,
                 `created_at` datetime,
                 `updated_at` datetime,
                 PRIMARY KEY (`id`)
              ) ENGINE=InnoDB DEFAULT CHARSET=latin1  

1b. Queries: queries/masters/countries.sql
              CREATE TABLE `countries` (
                 `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
                 `name` varchar(100),
                 `created_at` dateime,
                 `updated_at` datetime,
                 `flag` tinyint(1) DEFAULT 1,
                 PRIMARY KEY (`id`)
              ) ENGINE=InnoDB AUTO_INCREMENT=248 DEFAULT CHARSET=utf8

1c. Queries: queries/masters/states.sql
              CREATE TABLE `states` (
                 `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
                 `name` varchar(255) NOT NULL,
                 `country_id` mediumint(8) unsigned NOT NULL,
                 `created_at` timestamp NULL DEFAULT NULL,
                 `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                 PRIMARY KEY (`id`),
              ) ENGINE=InnoDB AUTO_INCREMENT=4029 DEFAULT CHARSET=utf8

1d. Queries: queries/masters/cities.sql
              CREATE TABLE `cities` (
                 `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
                 `name` varchar(255) NOT NULL,
                 `state_id` mediumint(8) unsigned NOT NULL,
                 `created_at` timestamp NOT NULL DEFAULT '2013-12-31 19:31:01',
                 `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                  PRIMARY KEY (`id`),
                ) ENGINE=InnoDB AUTO_INCREMENT=47953 DEFAULT CHARSET=utf8              

2.a. Controller : application/modules/masters/vendors.php
    
    <?php
class Vendors extends BaseController {
  public function __construct() {
    parent::__construct();
    $this->load->model(array('Country_model','State_model','City_model'));
  }

  	public function _get_dependent_associations(){
      $this->data['countries'] = $this->Country_model->get('id, name as name');          
      $this->data['states'] = array();
      $this->data['cities'] = array();



2.b. Controller : application/modules/masters/controllers/states.php

  <?php
    class States extends CI_Controller {
      public function __construct() {
        parent::__construct();
        $this->load->model(array('State_model')); 
      }
      
      public function index() {
        if (!empty($_GET['country_id'])) {
          $states = $this->State_model->get('id, name', array('country_id='.$_GET['country_id']));
          echo json_encode(array('states' => $states));
          exit;
        } else
          redirect_to(ADMIN_PATH);
      }
    }

2.c. Controller : application/modules/masters/controllers/cities.php

<?php
class Cities extends CI_Controller {
  public function __construct() {
    parent::__construct();
    $this->load->model(array('City_model')); 
  }

  public function index() {
    if (!empty($_GET['state_id'])) {
      $cities = $this->City_model->get('id, name', array('state_id='.$_GET['state_id']));
      echo json_encode(array('cities' => $cities));
      exit;
    } else
      redirect_to(ADMIN_PATH);
  }
}


3.JS : assets/application/js/masters/country_state_city.js

    	function set_state_options_on_change_of_country_id() {
      $("select[name*='country_id']").on('change', function() {
        $("select[name*='state_id']").val('');  
        set_state_options();
      });
    }

    function set_city_options_on_change_of_state_id() {
      $("select[name*='state_id']").on('change', function() {
        $(this).closest('.location_row').find("select[name*='city_id']").val('');
        set_city_options(this);
      });
    }

    function set_state_options() {
      var country_id = $(country_id_field).find("option:selected").val();
      $.ajax({
        type : 'GET',
        url : base_url+'masters/states?country_id='+country_id,
        dataType: 'JSON',
        success: function(response) {
          
          populate_state_options(response.states, country_id_field);
        },
        complete: function() {
          $("select[name*='state_id']").selectpicker('refresh');
          $("select[name*='city_id'] option").remove();
          $("select[name*='city_id']").selectpicker('refresh');
        }
      });
    }

    function populate_state_options(states, country_id_field) {
      $(country_id_field).closest('.location_row').find("select[name*='state_id'] option").remove();
      for (i=0; i < states.length; i++) {
        state_id = states[i].id;
        state_name = states[i].name;
        var option_html = "<option data-subtext='' value="+state_id+">"+state_name+"</option>";
        $(country_id_field).closest('.location_row').find("select[name*='state_id']").append(option_html);
      }
    }

    function set_city_options(state_id_field) {
      var state_id = $(state_id_field).find("option:selected").val();

      $.ajax({
        type : 'GET',
        url : base_url+'masters/cities?state_id='+state_id,
        dataType: 'JSON',
        success: function(response) {
          populate_city_options(response.cities, state_id_field);
        },
        complete: function() {
          $(state_id_field).closest('.location_row').find("select[name*='city_id']").selectpicker('refresh');
        }
      });
    }

    function populate_city_options(cities, state_id_field) {
      $(state_id_field).closest('.location_row').find("select[name*='city_id'] option").remove();
      for (i=0; i < cities.length; i++) {
        city_id = cities[i].id;
        city_name = cities[i].name;
        var option_html = "<option data-subtext='' value="+city_id+">"+city_name+"</option>";
        $(state_id_field).closest('.location_row').find("select[name*='city_id']").append(option_html);
      }
    }

4. form : application/modules/masters/views/vendors/form.php
  
  <form method="post" class="form-horizontal form-group-md form_radius_none" enctype="multipart/form-data"
      action="<?= get_form_action($controller, $action, $record) ?>">
    <div class="row location_row">
      <?php if ($action == 'edit' || $action == 'update'): ?>
        <?php load_field('hidden', array('field' => 'id')) ?>
      <?php endif; ?>
      <?php load_field('text', array('field' => 'name')) ?>
      <?php load_field('text',array('field' => 'code')) ?>
      <?php load_field('dropdown',array('field' => 'country_id',
                                        'option' => $countries)) ?>
      <?php load_field('dropdown',array('field' => 'state_id',
                                        'option' => $states)) ?>
      <?php load_field('dropdown',array('field' => 'city_id',
                                        'option' => $cities)) ?>
      </div>
      <?php load_field('submit', array('controller' => $controller)) ?>
  </form>

  <script type="text/javascript">
    var countries = <?= json_encode(get_records_by_id($countries)) ?>; 
  </script>

5. javascript : assets/application/js/base.js

  $(document).ready(function (){
    ......
    set_state_options_on_change_of_country_id();
    set_state_options_on_change_of_state_id();
  });



6. javascript : application/views/application/js.php
<script src="<?= ADMIN_LAYOUTS_PATH ?>js/masters/country_state_city.js"></script>
(to be added before base.js)