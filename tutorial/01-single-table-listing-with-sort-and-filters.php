Verified By: Atul
Verified On: 08-07-2019

Single table listing with sort and filters

Resource: Occupation
Module: masters
url: http://localhost/project/masters/occupations

1) Queries: 


2) Helper: application/helpers/modules/masters/occupations_helper.php

<?php defined('BASEPATH') OR exit('No direct script access allowed.');
function getTableSettings() {
  return array(
    'page_title'          => 'Occupations List',
    'primary_table'       => 'occupations',
    'default_column'      => 'id',
    'table'               => 'occupations',
    'join_columns'        => '',
    'join_type'           => '',
    'where'               => '',
    'where_ids'           => '',
    'order_by'            => '',
    'limit'               => "20",
    'extra_select_column' => 'id',
    'actionFunction'      => '',
    'headingFunction'     => 'list_settings',
    'search_url'          => 'occupations',
    'add_title'           => 'Add occupations',
    'export_title'        => '',
    'edit'                => '',
  );
}

/*
  0 => column title
  1 => column name
  2 => order flag
  3 => order column
  4 => filter flag
  5 => expand text flag
  6 => select column
  7 => exect match.
  8 => filter type  //static_dropdonw, dynamic_dropdown,static_multiselect,dynamic_multiselect, 
                     range,daterange,date,autocomplete, this parameter is also used to show image pass image
  9 => filter type array //if static set value need to show array(1,2,3).
        if dynamic filter write query as we do before make alias what we taken on 1st index. if image set path of image.
  10 => for default image full path                      
*/

/*expample = array("Professional Name", "professional_name", true, "professional_name", true, false,'professional_name','Professional',false,'dynamic_dropdown','SELECT DISTINCT(`name`) as professional_name FROM users'),*/
function list_settings() {
  return array(
    array("Name", "name", TRUE, "name", TRUE, TRUE),
    array("Alias", "alias", TRUE, "Alias", TRUE, TRUE),
    array("Created At", "created_at", TRUE, "created_at", TRUE, TRUE),
    array("Updated At", "updated_at", TRUE, "updated_at", TRUE, TRUE),
    array("Action", "action", FALSE, "action", FALSE, FALSE),
  );
}
  

3) Controller: application/modules/masters/Occupations.php

<?php
class Occupations extends BaseController {
  public function __construct(){
    parent::__construct();
  }
}


4) Model: application/modules/masters/models/Occupation_model.php

<?php
class Occupation_model extends BaseModel{
  protected $table_name = 'occupations';
  protected $id = 'id';
  public function __construct() {
    parent::__construct();
  }
}