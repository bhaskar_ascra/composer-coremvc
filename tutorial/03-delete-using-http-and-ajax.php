Notes: Migration and Controller Code to be added

Delete - using HTTP and AJAX

Resource: School
Module: masters
url: http://localhost/project/masters/schools

1) Helper: application/helpers/modules/masters/school_helper.php
<?php defined('BASEPATH') OR exit('No direct script access allowed.');

function get_row_actions($row, $url, $select_url, $filter) {
  $actions = array();
  $actions["delete_and_redirect"] = array('request' => "http", 
                                          'url' => ADMIN_PATH.'masters/schools/delete/'.$row['id'],
                                          'confirm_message' => "Do you want to Delete?",
                                          'class' => 'text-danger');

  $actions["delete_using_ajax"] = array('request' => "js", 
                                        'url' => ADMIN_PATH.'masters/schools/delete/'.$row['id'],
                                        'confirm_message' => "Do you want to Delete?",
                                        'js_function =>"",
                                        'class' => 'text-danger');
  return $actions;
}
