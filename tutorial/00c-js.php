JS Tutorial
url: https://www.w3schools.com/js/js_type_conversion.asp

JS HOME
  JS Introduction
  JS Arrays
  JS Array Methods
  JS Array Sort
  JS Array Iteration
  JS Dates
  JS Date Formats
  JS Date Get Methods
  JS Date Set Methods
  JS Math
  JS Random
  JS Booleans
  JS Comparisons
  JS Conditions
  JS Switch
  JS Loop For
  JS Loop While
  JS Break
  JS Type Conversion