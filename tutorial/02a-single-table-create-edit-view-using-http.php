Notes: queries missing


Create, Edit, View for Simple Table

Resource: Industries
Module: masters
url: http://localhost/project/masters/industries


1) Helper: application/helpers/modules/masters/industries_helper.php

function list_settings() {
  return array(
    ...
    ...
    array("Action", "action", FALSE, "action", FALSE, FALSE),
  );
}

function get_field_attribute($table, $field) {
  $attributes = array();
  $attributes['industries'] = array(
    'id' => array('', '', FALSE, '', FALSE),
    'name'  => array('Name', 'Enter Name of Role', TRUE, '', TRUE),
    'alias'  => array('Alias', 'Enter Name of Role', TRUE, '', TRUE),

  );
  return $attributes[$table][$field];
}

function get_row_actions($row, $url, $select_url, $filter) {
  $actions = array();
  $actions["Edit"] = array('request' => "http",
                                  'url' => ADMIN_PATH.'masters/industries/edit/'.$row['id'],
                                  'confirm_message' => "",
                                  'class' => 'btn_green');
  return $actions;
}


2) Controller: application/modules/masters/Industries.php

<?php
class Industries extends BaseController{
  public function __construct() {
    parent::__construct();
    $this->redirect_after_save = 'view';
  } 
}


3) Model: application/modules/masters/models/Industry_model.php

<?php
class Industry_model extends BaseModel {
  protected $table_name = 'industries';
  protected $id = 'id';
  public function __construct() {
    parent::__construct();
  }
}


4) Views: application/modules/masters/views/industries/form.php

<form method="post" class="form-horizontal form-group-md form_radius_none" enctype="multipart/form-data"
      action="<?= get_form_action($controller, $action, $record) ?>">
  <div class="row">
    <?php if ($action == 'edit' || $action == 'update'): ?>
     <?php load_field('hidden', array('field' => 'id')) ?>
    <?php endif; ?>
    <?php load_field('text', array('field' => 'name')) ?>
    <?php load_field('text',array('field' => 'alias')) ?>
  </div>
  <?php $this->load->view('forms/fields/submit', array('controller' => $controller)) ?>
</form>


5) Views: application/modules/masters/views/industries/view.php

<h5 class="heading">Basic Details</h5>
<a href=<?= ADMIN_PATH.'masters/industries/edit/'.$record['id'] ?>>Edit</a>
<div class="row">    
  <?php load_field('label_with_value', array('field' => 'name')) ?>
 
  <?php load_field('label_with_value', array('field' => 'alias')) ?>
</div>
