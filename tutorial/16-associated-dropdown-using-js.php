Associated Dropdown with JS

Resource: Proceedings, Matters, Events
Module: events

1a. Queries: queries/events/proceedings.sql
CREATE TABLE IF NOT EXISTS `proceedings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

INSERT INTO `proceedings` (`id`, `name`, `created_at`, `updated_at`, `created_by`) VALUES
(1, 'Proceeding 1', '2019-04-02 20:02:18', '2019-05-12 18:33:12', NULL),
(2, 'Proceeding 2', '2019-04-03 11:33:29', '2019-05-12 18:33:30', NULL),
(3, 'Proceeding 3', '2019-04-03 14:40:45', '2019-05-12 18:33:37', NULL),
(4, 'Proceeding 4', '2019-04-08 11:32:59', '0000-00-00 00:00:00', NULL);

1b. Queries: queries/events/matters.sql
CREATE TABLE IF NOT EXISTS `matters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `proceeding_id` int(11) NOT NULL,
  `status` varchar(50) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

INSERT INTO `matters` (`id`, `name`, `proceeding_id`, `status`, `created_at`, `updated_at`, `created_by`) VALUES
(1, 'Matter 1', 1, 'Open', '2019-04-02 20:02:26', '2019-04-08 11:40:45', NULL),
(2, 'Matter 2', 1, 'Open', '2019-04-03 14:41:24', '0000-00-00 00:00:00', NULL),
(3, 'Matter 3', 1, 'Open', '2019-04-12 15:29:22', '0000-00-00 00:00:00', NULL),
(4, 'Matter 4', 1, 'Closed', '2019-04-12 15:30:40', '2019-05-27 18:11:18', NULL),
(5, 'Matter 5', 2, 'Open', '2019-05-12 18:58:21', '2019-05-12 18:58:21', NULL),
(6, 'Matter 6', 2, 'Closed', '2019-05-13 15:39:09', '2019-05-25 00:38:29', NULL),
(7, 'Matter 7', 2, 'Open', '2019-05-14 15:53:18', '2019-05-14 15:54:19', NULL),
(8, 'Matter 8', 3, 'Open', '2019-05-15 12:23:40', '2019-05-15 12:23:40', NULL),
(9, 'Matter 9', 3, 'Open', '2019-05-15 12:34:42', '2019-05-15 12:34:42', NULL),
(10, 'Matter 10', 3, 'Open', '2019-05-15 15:37:18', '2019-05-15 15:37:18', 6),
(11, 'Matter 11', 4, 'Open', '2019-05-15 18:24:01', '2019-05-15 18:24:01', 0),
(12, 'Matter 12', 4, 'Open', '2019-05-15 18:24:11', '2019-05-15 18:24:11', 11),
(13, 'Matter 13', 4, 'Closed', '2019-05-16 12:00:05', '2019-05-22 20:41:10', 0);

1c. Queries: queries/events/events.sql
CREATE TABLE IF NOT EXISTS `events` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `matter_id` int(11) NOT NULL,
  `event_name` varchar(50) NOT NULL,
  `event_type` varchar(50) NOT NULL,
  `event_section_id` varchar(20) DEFAULT NULL,
  `proceeding_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=296 ;

2. Controllers: application/modules/events/Events.php
<?php
class Events extends BaseController {
  public function __construct() {
    parent::__construct();     
    $this->load->model(array('Proceeding_model', 'Matter_model'));
  }
  
  public function _get_dependent_associations() {  
    $this->data['proceedings_list'] = $this->Proceeding_model->get('id, name'); 
    $this->data['matters_list'] = $this->Matter_model->get('id, name, proceeding_id');
  }
}
?>

3. Helpers: applications/helpers/events/Events_helper.php
<?php defined('BASEPATH') OR exit('No direct script access allowed.');
function get_field_attribute($table, $field) {
  $attributes = array();
  $attributes = array(
    'proceeding_id'  => array('Proceeding', '', TRUE, '', TRUE),
    'matter_id' => array('Matter', '', TRUE, '', TRUE),
  );
  return $attributes[$field];
}
?>

4a. Model: application/modules/events/Event_model.php
<?php
class Event_model extends BaseModel {
  protected $table_name = 'events';
  protected $id = 'id';
  public function __construct() {
    parent::__construct();
  }
}

4b. Model: application/modules/events/Proceeding_model.php
<?php
class Proceeding_model extends BaseModel {
  protected $table_name = 'proceedings';
  protected $id = 'id';
  public function __construct() {
    parent::__construct();
  }
}

4c. Model: application/modules/events/Matter_model.php
<?php
class Matter_model extends BaseModel {
  protected $table_name = 'matters';
  protected $id = 'id';
  public function __construct() {
    parent::__construct();
  }
}

5. View: application/modules/events/events/form.php
<form method="post" class="form-horizontal form-group-md form_radius_none" enctype="multipart/form-data"
      action="<?= get_form_action($controller, $action, $record) ?>">
  <?php $this->load->view('forms/fields/select_select', array('field' => 'proceeding_id',
                                                              'option' => $proceedings_list)); ?>

  <?php $this->load->view('forms/fields/select_select', array('field' => 'matter_id',
                                                              'option' => $matters_list)); ?>
  <?php $this->load->view('forms/fields/submit', array('controller' => $controller,
                                                       'submit_label' => (empty($record['id'])) ? 'NEXT' : 'SAVE')) ?>
</form>
<script>
  var proceedings = <?= json_encode(get_records_by_id($proceedings_list)) ?>; 
  var matters = <?= json_encode(get_records_by_id($matters_list)) ?>;
</script>

6a. Javacript: assets/application/js/events/events.js
function set_matter_option_on_change_of_proceeding_id() {
  $("select[name*='proceeding_id']").on('change', function() {
    $("select[name*='matter_id']").val('');
    set_matter_options();
  });
}

function set_matter_options() {
  if (window.matters==undefined) {
    return;
  }

  $("select[name*='matter_id'] option").hide();
  var proceeding_id = $("select[name*='proceeding_id']").find("option:selected").val();
  for (var key in matters) {
    if (matters[key]['proceeding_id']==proceeding_id) {
      $("select[name*='matter_id'] option[value="+matters[key]['id']+"]").show(); 
    }
  }
  $("select[name*='matter_id']").selectpicker('refresh');
}

6b. Javacript: assets/application/js/base.js
$(document).ready(function (){
  ...........
  set_matter_options();
  set_matter_option_on_change_of_proceeding_id();
}

6b. Javascript: application/views/layout/application/js.php
<script src="<?= ADMIN_LAYOUTS_PATH ?>js/masters/events.js"></script>
(to be added before base.js)