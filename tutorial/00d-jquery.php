jQuery Tutorial
url: https://www.w3schools.com/jquery/default.asp

jQuery HOME
  jQuery Intro
  jQuery Get Started
  jQuery Syntax
  jQuery Selectors
  jQuery Events

jQuery Effects
  jQuery Hide/Show
  jQuery Fade
  jQuery Slide
  jQuery Animate
  jQuery stop()
  jQuery Callback
  jQuery Chaining

jQuery HTML
  jQuery Get
  jQuery Set
  jQuery Add
  jQuery Remove
  jQuery CSS Classes
  jQuery css()
  jQuery Dimensions

jQuery Traversing
  jQuery Traversing
  jQuery Ancestors
  jQuery Descendants
  jQuery Siblings
  jQuery Filtering

jQuery AJAX
  jQuery AJAX Intro
  jQuery Load
  jQuery Get/Post

jQuery Misc
  jQuery noConflict()
  jQuery Filters

jQuery Examples
  jQuery Examples
  jQuery Quiz
  jQuery Exercises

jQuery References (to be referred when required)
  jQuery Overview
  jQuery Selectors
  jQuery Events
  jQuery Effects
  jQuery HTML/CSS
  jQuery Traversing
  jQuery AJAX
  jQuery Misc
  jQuery Properties
