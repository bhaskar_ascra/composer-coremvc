=========================== Controller ======================================================

class Company_sizes extends BaseController {
  public function __construct() {
    parent::__construct();
    $this->load->library('excel_lib'); 
    $this->data['errors']=array();
    $this->data['import_table_names'] = array('company_sizes'); //table name 
  }

  public function store() {
    $this->data['validation_klass']='import_file';
    $client_id=$_POST[$this->router->class]['client_id'];
    $this->data['redirect_url']=base_url($this->router->module.'/'.$this->router->class."?client_id=".$client_id);
    parent::store();
  }
}

================================ End of Controller ===============================================



// ================================= Model =========================================================
<?php
class Company_size_model extends BaseModel{
  protected $table_name = 'company_sizes';
  protected $id = 'id';
  public function __construct($data=array()) {
    parent::__construct($data);
  }
  

  public function validation_rules($klass='import_file') {
    $rules = array();
    $rules['import_file'] = array(
      array('field'  => 'company_sizes[import_files]', 'label' => 'Upload File',
            'rules'  => array('trim',
                        array('validate_file',array($this,'required_file')),
                        array('file_extension_validate',array($this,'file_extension_validate'))),
            'errors' => array("validate_file" => "Please Upload required file",
                              "file_extension_validate" => "Please provide valid file of extension xls,xlx")));

    $rules['record'] = array(
      array('field' => 'company_sizes[from_size]', 'label' => 'From Size',
            'rules' => 'trim|required|numeric',
            'errors'=>array('required'=>'From size')),
      array('field' => 'company_sizes[to_size]', 'label' => 'To Size',
            'rules' => 'trim|required|numeric',
            'errors'=>array('required'=>'To size')));
    return $rules[$klass];
  }

  public function required_file($str,$index='') {
    if(!empty($_FILES[$this->router->class]['name']['import_files'])){
      return true;
    }else{
      return false;
    }
  }

  public function file_extension_validate($str,$index='') {
    if(!empty($_FILES[$this->router->class]['name']['import_files'])) {
      $extension = strtoupper(pathinfo($_FILES[$this->router->class]['name']['import_files'], PATHINFO_EXTENSION));
        if($extension != 'XLS' && $extension != 'XLSX')
          return false;
        else
          return true;
    }else{
      return false;
    }
  } 
}

//================================ End of Model ==================================================