Notes: Controllers and forms code to be added

Unique Validation

Resource: User
Module: users

1. Model: application/modules/users/models/User_model.php
<?php
class User_model extends BaseModel {
  protected $table_name = 'users';
  protected $id = 'id';

  public function __construct() {
    parent::__construct();
  }

  public function validation_rules($klass='') {
    return array(
      array('field' => 'users[name]', 'label' => 'User Name', 'rules' => 'trim|required'),
      array('field' => 'users[email]', 'label' => 'Email',
            'rules' => array('trim', 'required', 
                       array('unique_email', array($this->User_model, '_is_email_unique'))),
            'errors'=>array('unique_email' => "Email already exists.")));
  }

  public function _is_email_unique($email) {
    $where_conditions = array('email="'.$email.'"');
    if(isset($_POST['users']['id']))
      array_push($where_conditions, 'id !='.$_POST['users']['id']);
    $result = $this->get('id', $where_conditions);
    return (empty($result));
  }
}



