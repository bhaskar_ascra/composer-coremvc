10. One to Many Association
- create and edit
- validation
- form with add more using Javascript
- deleting association records

Resource: Transactions, Transaction_details
Module: masters

1a. Query: queries/transactions.sql
CREATE TABLE IF NOT EXISTS `transactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `date` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
);


1b. Query: queries/transaction_details.sql
CREATE TABLE IF NOT EXISTS `transaction_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_code` varchar(255) NOT NULL,
  `quantity` int(11) NOT NULL,
  `weight` float NOT NULL,
  `category_id` int(10) NOT NULL,
  `transaction_id` int(10) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
);


2. Controller: application/modules/masters/Transactions.php
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transactions extends BaseController {
  public function __construct(){
    parent::__construct();
    $this->redirect_after_save = 'view';
    $this->load->model(array('Transaction_detail_model'));
  } 
  
  //populate transaction_details in the form.php
  public function _get_dependent_associations(){
    if ($this->router->method=='create')
      $this->data['transaction_details'] = array();
    elseif ($this->router->method=='edit')
      $this->data['transaction_details'] = $this->Transaction_detail_model->get('', array('transaction_id='.$this->data['record']['id']));
    else
      $this->data['transaction_details'] = $_POST['transaction_details'];
  }
}

3. Helper:  application/helpers/modules/masters/transactions_helper.php
<?php defined('BASEPATH') OR exit('No direct script access allowed.');
function get_field_attribute($table, $field) {
  $attributes = array();
  $attributes['transactions'] = array(
    'id' => array('', '', FALSE, '', FALSE),
    'name'  => array('Order Name', 'Enter Order Name', TRUE, '', TRUE),
    'date'  => array('Date', 'Select Date', TRUE, '', TRUE),
  );

  $attributes['transaction_details'] = array(
    'id' => array('', '', FALSE, '', FALSE),
    'item_code'  => array('', '', TRUE, '', TRUE),
    'quantity'  => array('', '', TRUE, '', TRUE),
    'weight'  => array('', '', TRUE, '', TRUE),
    'category_id'  => array('', '', TRUE, '', TRUE),
    'delete' => array('', '', FALSE, '', FALSE)
  );
  return $attributes[$table][$field];
}

4a. Model: application/modules/masters/Transaction_model.php
<?php
class Transaction_model extends BaseModel{
  protected $table_name = 'transactions';
  protected $id = 'id';
  public function __construct() {
    parent::__construct();
  }

  public function save_association_data($data, $action){
    $transaction_details = $_POST['transaction_details'];
    foreach($_POST['transaction_details'] as $index => $transaction_detail) {
      $transaction_detail['transaction_id'] = $data['id'];
      if($transaction_detail['delete']==1 && !empty($transaction_detail['id']))
        $this->Transaction_detail_model->delete($transaction_detail['id']);  
      unset($transaction_detail['delete']);
      $this->Transaction_detail_model->save($transaction_detail);
    }
  }

  public function validate($data, $validation_klass='') {
    $rules = $this->validation_rules();
    foreach($data['transaction_details'] as $index => $transaction_detail) {
      $transaction_detail_rules = $this->Transaction_detail_model->validation_rules('', $index);
      $rules = array_merge($rules, $transaction_detail_rules);
    }
    $this->form_validation->set_rules($rules);
    $this->form_validation->set_data($data);
    return $this->form_validation->run();
  }

  public function validation_rules($klass='') {
    $rules = array(array(
      'field' => 'transactions[name]',
      'label' => 'Name',
      'rules' => array('trim', 
                       'required', 
                       'max_length[45]')));
    return $rules;
  }
}

4b. Model: application/modules/masters/Transaction_detail_model.php
<?php
class Transaction_detail_model extends BaseModel{
  protected $table_name = 'transaction_details';
  protected $id = 'id';
  public function __construct() {
    parent::__construct();
  }

  public function validation_rules($klass='', $index=0) {
    $rules = array(array( 
      'field' => 'transaction_details['.$index.'][item_code]',
      'label' => 'Name',
      'rules' => array('trim', 
                       'required', 
                       'max_length[5]')));
    return $rules;
  }
}


5a. View: application/modules/masters/views/transactions/form.php
<form method="post" class="form-horizontal form-group-md form_radius_none" enctype="multipart/form-data"
      action="<?= get_form_action($controller, $action, $record) ?>">
  <div class="row">
    <?php if ($action == 'edit' || $action == 'update'): ?>
      <?php load_field('hidden', array('field' => 'id')) ?>
    <?php endif; ?>
    <?php load_field('text', array('field' => 'name')) ?>                 
    <?php load_field('date', array('field' => 'date')) ?>
  </div>
  <table border="1" width="100%" style="text-align: center;" class="table-bordered">
    <tr>
      <th>Item_Code</th>
      <th>Quantity</th>
      <th>Weight</th>
      <th>Category ID</th>
    </tr>
    <tbody id="transaction_detail">
      <?= getJsButton('Add', '#', '', '', 'add_transaction_detail_subform()'); ?>
      <?php 
        foreach ($transaction_details as $index => $transaction_detail) {
          $this->load->view('transaction_details/subform.php', array('index' => $index));
        } 
      ?>
    </tbody>
  </table>
  <?php load_field('submit', array('controller' => $controller)) ?>
</form>

<script>
  <?php $transaction_detail_form_html =  $this->load->view('transaction_details/subform', 
                                                     array('index' => 'index_count'), TRUE); ?>
  var transaction_detail_form_html = <?= json_encode(array('html' => $transaction_detail_form_html)); ?>;
  var fields_index = <?= time() ?>;
  
  function add_transaction_detail_subform() {
    var html_str = transaction_detail_form_html.html.replace(/\index_count/g, fields_index);
    fields_index += 1;
    $('#transaction_detail').append(html_str);
    return false;
  }
</script>


5b. View: application/modules/masters/views/transaction_details/subform.php
<tr class="transaction_details_<?= $index ?>">
  <td>
    <?php load_field('hidden', array('field' => 'id',
                                     'index' => $index,
                                     'controller' => 'transaction_details')); ?>

    <?php load_field('text', array('field' => 'item_code',
                                   'index' => $index,
                                   'controller' => 'transaction_details')); ?>
  </td>
  <td>
    <?php load_field('text', array('field' => 'quantity',
                                   'controller' => 'transaction_details',
                                   'index' => $index)); ?>
  </td>
  <td>
    <?php load_field('text', array('field' => 'weight',
                                   'controller' => 'transaction_details',
                                   'index' => $index)); ?>
  </td>
  <td>
    <?php load_field('text', array('field' => 'category_id',
                                   'controller' => 'transaction_details',
                                   'index' => $index)); ?>
  </td>
  <td>
    <?= getJsButton('Delete', '#', '', '', 'delete_transaction_detail('.$index.')'); ?>
    <?php load_field('hidden', array('field' => 'delete',
                                     'controller' => 'transaction_details',
                                     'index' => $index)); ?>
  </td>
</tr> 

6a. Javascript: assets/application/js/masters/transactions.js
function delete_transaction_detail(index) {
  $("input[name*='transaction_details["+index+"][delete]']").val(1);
  $("tr.transaction_details_"+index).hide();
}

6b. Javascript: application/layouts/application/js.php
<script src="<?= ADMIN_LAYOUTS_PATH ?>js/masters/transaction_details.js"></script>
(to be added before base.js)